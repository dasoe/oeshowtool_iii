## VPT8
[Video Projection Tool (VPT)](https://hcgilje.wordpress.com/vpt/) is a free multipurpose realtime projection software tool by HC Gilje. 
I used this incredible piece of software for driving quite some shows and I can only recommend it. 
You can use it to set your screens, masks, mappings of course. But you can also use scenes, include external sources as well as hardware, script/program nearly everything... It is incredibly versatile as well as mature. You can use it to set up fully automated projections in museum context, drive shows in theatre plays, that are prepared but triggered manually or manually drive live shows that you set up before.
And it's free.  
The reason why I compare it to my tool is not to put it down. At all! It's not about bad or good, just about different approaches. Also it has been the closest to what I need for years, it's probably the only tool I know that _can_ reasonably be compared at all.  

**Thanks a lot!**

## oeShowTool
oeShowTool is a new tool and I write it in my spare time. It doesn't have all the planned features yet. It was not written to compete with VPT and does not aim to do the same. It was written to fulfill a _specific_ need, that I have not found in other software.  
It's meant to drive live shows that can be prepared to a certain extent. But main focus is on being able to set things up quickly and doing most of the work live - while still getting a decent show.  
It is more or less meant to empower you to **improvise** while using technology.

### Why I wrote this tool / for whom it may be useful
I often work with subculture people on subculture events in subculture spaces. The approach often is not very professional in a classical sense. It's rather: "let's get in and then see what we'll do". Let's imrovise. In advance, I will not know where I can hang (actually often _put_) my projectors, will not know the sizes of the room or which parts of the location we will actually be using, I will not know the light situation, I will not know anything. And I'll only have few hours to prepare everything - which here often includes organizing cables to even have electricity. You get the point.  
This approach is very much at odds with the idea of many tools to _set up a show in advance_ (honestly, many tools expect you to feed them a 3D model of the room). So we needed something new. 

The best by far we found was VPT. But even this wonderful tool is optimized for something else, so in the end, my own tool evolved.

## Comparison/Approaches
Some of this is only to be understood, when you know VPT probably. Also I am aware just how mighty VPT is. So there might also be solutions inside VPT for some of my problems, putting a little bit of effort in... Anyway:

### Seperation of scenes and screens
In VPT8 _presets_ are stored (saved) that include layers (virtual screens), their position and mapping as well as sources (what to show on which layer). This makes a lot of sense in prepared shows but is the main reason, why I decided to write my own tool with another approach: 

With oeShowTool, scenes and screens are saved seperately. It is possible to prepare multiple scenes (again: what to show on which screen) in advance for a (guessed) number of screens  and save them. The screens are saved independently and can so be adjusted (position, mapping) later (on site) without interfering with the scenes. If you change a screen position, the screen will be at a different place for all scenes. And if you decide to add another screen, it will be available in all scenes, ready to be used, 

### Seperation of shown content and slots
VPT8 uses _slots_ that hold specific sources (images, videos). A slot is then connected to a screen and the content of the slot is shown. Slots have the ability to fade and presets (scenes) can fade from one slot to another. The slots contents are also saved with the scenes mentioned above. 
Again, there's no problem in ths approach for well-prepared shows. For driving badly unprepared shows, this can lead to problems, though: If I click on a scene, the scene will often try to fade from one slot to another. If I have manually changed a used slot in between (what I need for my live shows, as they are not completely prepared), the scene will change the slot back first, before fading.
So saved scenes is:  
picture of lake -smooth fade-> picture of Tree.   
I manually changed the lake to a house in between. I expect:  
picture of house -smooth fade-> picture of tree.  
But I get:   
picture of house -HARD CHANGE- picture of lake -smooth fade-> picture of tree.  
I can prevent this by doing it another way - if I'm always fully alert and keep in mind future problems. But in stressful show situations, it adds another level of complexity.

Hence in oeShowTool, I seperated slots from shown content. It is always a screen holding an _actual_ content and an optional _target content_. Slots exist here too, but they only pass target content to a screen. Actual content of a screen is never touched, so unvoluntary hard changes can not happen.

### Screen-spanning, one-window, embedded GUI
VPT8 uses multiple windows to control the show and one window, that can be extended to fullscreen showing the actual virtual screens.
When using multiple projectors (which nowadays is more and more affordable), you have to rely on some specific software (like Eyefinity on AMD cards or Vision Surround on NVIDIA cards) to combine multiple monitors/projectors.

oeShowTool uses one big window, spanning as many monitors/projectors as desired. Ususally the first screen (monitor) holds GUI windows, but those can be freely distributed, also to other screens (projectors) - which can also help in setting things up (somtimes you might want to see the GUI next to a virtual screen). 

### Cross platform

Last but not least: VPT8 is written using Max. This means it will only run on Mac and Windows. oeShowTool is written with openFrameworks. It's open source and can be compiled for a lot of platforms. It can also be adjusted by anyone, as all used software and libraries are free and open source as well.
