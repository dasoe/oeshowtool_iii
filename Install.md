# Install oeShowTool III

This tool is aimed at professionals. There is no one-click installer and I doubt, I will find the time to build one. I apologize for this.
I also do not publicly provide a zip file containing all the external software needed (I can actually do that if you send me a PM). This is a license issue, but also a matter of respect for the developers.

## Windows

For Windows you'll need 4 Steps.
1. Get the Tool itself
2. Install Windows C++ redistributables (don't ask...)
3. Install a Codec pack to have virtually any Video codec available.
4. Provide gstreamer, a tool to handle videos more efficiently than Windows does internally.

First three steps are easy and quickly done. 
The last step *should* be easy, but sometimes is not. So I provide 2 possibilities, that you can choose from.


Let's go:

### 1. Get the tool.

- Starting with Version 0.2 you can just get a zip file from [latest release](../../releases). 
- Unpack the file.
- Inside the folder there's a file called `oeShowTool_III.exe`, which starts the Tool - once the following requirements are met:

### 2. Install Windows C++ redistributables
You'll find a link at the [latest release](../../releases), too. 
Just download the linked version and install.  

### 3. Install K-Lite Codec pack
Just the same. you'll find a link at the [latest release](../../releases). 
Please download the linked version and install.  

### 4. Install gstreamer
You have 2 posiibilities to choose from, both work:

#### 4A: Quick and easy: just put gstreamer files in your folder

Inside the folder of the tool, you'll find the file `for_windows_extract_content_here.zip`. Just unpack it and put the contents (yes, all the files) in the folder of the tool.

You see: this is easy, but is also flooding your folder with even more files. So you might prefer this version:

#### 4B: More professional, but *potentially* more complicated: Install gstreamer on your system

You'll find a link at the [latest release](../../releases), too. 
- Please download the noted version (here `gstreamer-1.0-x86_64-1.6.4.msi` -. better stick to it, newer is not better here!) and install.  
- Test the Install by going to Windows shell (cmd) or Powershell and typing `gst-play-1.0`. 
  - In case you see `Usage: gst-play-1.0 FILE1|URI1 [FILE2|URI2] [FILE3|URI3] ...`, all ist well and you're done.
  - In case you see something like `gst-play-1.0.' is not recognized as an internal or external command`, the path variable has not been properly set (thanks, Windows!) and you'll have to do this yourself.
      - Go to Environment Variables (go to search and type 'envir', this should already show the appropriate settings window)
      - Add `C:\gstreamer\1.0\x86_64\bin` to your path variable. Test again, if it does not work log out and in again, do a restart and so on (thanks again Windows!)

As soon as you see the correct output from `gst-play-1.0`, you're finally done.



## Linux
