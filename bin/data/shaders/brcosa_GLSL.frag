//
// Fragment shader for modifying image contrast by
// interpolation and extrapolation
//
// Author: Randi Rost
//
// Copyright (c) 2002: 3Dlabs, Inc.
//
// See 3Dlabs-License.txt for license information
//

const vec3 LumCoeff = vec3 (0.2125, 0.7154, 0.0721);

varying vec2 texcoord;
uniform sampler2DRect image;
uniform sampler2DRect imageMask;

uniform vec3 avgluma;
uniform float saturation;
uniform float contrast;
uniform float brightness;
uniform float alpha;
uniform vec3 tintColor;

void main (void)
{
vec3 texColor = texture2DRect(image, texcoord).rgb;
float ownAlpha = texture2DRect(imageMask, texcoord).a;
vec3 intensity = vec3 (dot(texColor, LumCoeff));
vec3 color = mix(intensity, texColor, saturation);
vec4 tttColor = texture2DRect(image, texcoord).rgba;
color = mix(avgluma, color, contrast);
color *= brightness;

vec3 tintIntensity = vec3 (dot(tintColor, LumCoeff));

float testBrightness = (color.r + color.g + color.b) /3.0;

color = mix(color,tintColor,tintIntensity);

if (alpha < ownAlpha) {
ownAlpha = alpha;
}

gl_FragColor = vec4 (color, ownAlpha);
}
