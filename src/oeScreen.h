#pragma once
#include "ofMain.h"
#include "ofxBezierWarp.h"
#include "oeUtilities.h"
#include "oeExternalSource.h"


class oeScreen : public ofxBezierWarp {
    public:    
    oeScreen();
    void init(int width, int height);
    void setWidth(int transWidth);
    void setHeight(int transHeight);

    
    //virtual ~oeScreen();
    void oedraw();
    void oedraw(float x, float y, float w, float h);

    void update();    
    void drawContent();    
    int getSourceType();
    int getTargetSourceType();
    string getTargetSourceInfo();
    string getSourceInfo();
    string getPath();
    int getFadeTime();    
    ofFloatColor getColor();
    void setMasterAlpha(int talpha);
    int getMasterAlpha();

    void setTargetVideoLoopState(ofLoopType state);
    void setVideoLoopState(ofLoopType state);
    ofLoopType getVideoLoopState();
    bool getVideoPaused();
    float getVideoSpeed();
    void setVideoSpeed(float speed, float masterSpeed);
    float getVideoPosition ();
    void setVideoPosition(float position);

    void setMask(string transPath);
    ofImage getMask();
    string getMaskPath();
    void setMaskPreview(ofImage transMaskPreview);
    void removeMaskPreview();
    
    
    void prepareContent(); 
    
    ofFbo contentFbo;
    ofShader brcosaShader;

    
    void setTargetSource(int transType, string transPath, ofFloatColor transColor, int transFadeTime, ofLoopType transLoop, float transSpeed, float masterSpeed, float masterAlpha);
    void startTransition();
    
    ofLoopType loopState;
    float speed, masterSpeed, baseSpeed;
    int sourceType, targetSourceType;
    string path, targetPath;    
    ofFloatColor color[2];
    ofVideoPlayer video[2];
    ofImage image[2];
    oeExternalSource external[2];
    bool mainColor, mainImage, mainVideo, mainExternal;

    float brightness, contrast, saturation;
    ofVec3f tintColor;

    
    ofImage mask, maskPreview;
    string maskPath;
    
    int targetAlpha;
    float masterAlpha, targetMasterAlpha, oneStepMasterAlpha;

    long actualTime, fadeTimerForSlot;
    int fadeTimeForSlot;
    
    oeUtilities oeUtil;

 
};