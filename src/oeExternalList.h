#pragma once
#include "ofMain.h"

#ifdef TARGET_WINVS
    #include "ofxSpout.h"
#endif

#ifdef USE_NDI
    #include "ofxNDI.h" // NDI classes
#endif


class oeExternalList
{
public:
    void init();    
    vector<std::string> list();
    void exit();

    //map< string, vector<std::string> > sourcesLists; 

#ifdef TARGET_WINVS
    ofxSpout::Receiver spoutReceiver;
#endif

#ifdef USE_NDI
    ofxNDIreceiver ndiReceiver;
#endif
    
};

