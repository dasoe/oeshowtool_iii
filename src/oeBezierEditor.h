#pragma once
#include "ofMain.h"
#include "ofxBezierEditor.h"
#include "ofxOpenCv.h"

class oeBezierEditor {
public:


    void init(int contentWidthTemp, int contentHeightTemp, int maskWidthTemp, int maskHeightTemp);
    void update();
    void draw();
    void drawHelp();

    void setActive(bool active);
    bool getActive();
    void loadBezier(string maskName);
    void saveBezier(string path);

    void saveMask(int blur);
    ofImage grabMask(int blur);
    bool getEditBezier();
    int contentWidth, contentHeight, maskWidth, maskHeight;


    ofxCvColorImage cvImage;

    ofImage screenGrab;
    ofxBezierEditor myBezier;
    bool isActive;
    ofFbo maskFBO;
};
