#include "ofApp.h"
#include "imgui.h"
#include "imgui_internal.h"

//--------------------------------------------------------------

void ofApp::setup( ) {

    // Hack for Ubuntu window Initialization (let some time elapse before ofSetFullscreen works)
    windowInited = 0;

    // directories
    listDirectories( );
    ofSetFrameRate( 60 );
    //TIME_SAMPLE_SET_FRAMERATE(60.0f); //specify a target framerate

    // gui
    gui.setup( nullptr, true, ImGuiConfigFlags_DockingEnable , true );

    // some settings
    sceneCounter = 10;
    cycledUpdateTimeDelta = 400;
    startSlotColor = ImColor( 114, 144, 154, 200 );

    settings.addInt( "firstMonitorWidth", 1024 );
    settings.addInt( "firstMonitorHeight", 768 );
    settings.addInt( "screenWidth", 1920 );
    settings.addInt( "screenHeight", 1080 );
    settings.addInt( "numberOfScreens", 2 );
    settings.addInt( "contentWidth", 1024 );
    settings.addInt( "contentHeight", 768 );
    settings.addInt( "maskWidth", 1024 );
    settings.addInt( "maskHeight", 768 );
    settings.addBoolean( "fullScreen", 0 );
    settings.addFloat( "startMasterSpeed", 1.0 );
    settings.addBoolean( "drawGuiAtBeginning", 1 );
    settings.addBoolean( "useFeedbackWindow", 1 );
    settings.addVec2f( "feedbackWindowSize", ofVec2f( 1000, 500 ) );
    settings.addString( "defaultMask", "masks/defaultMask.jpg" );
    settings.addBoolean( "useMidi", 1 );
    settings.addInt( "midiPort", 1 );

    settings.init( "settings.xml", true );

    ofLogNotice( "prepare window!" );


    useFeedbackWindow = settings.getBooleanValue( "useFeedbackWindow" );
    drawGui = settings.getBooleanValue( "drawGuiAtBeginning" );
    
    useMidi = settings.getBooleanValue( "useMidi" );
    midiPort = settings.getIntValue( "midiPort" );

    if ( useMidi ) {
        // print input ports to console
        midiIn.listInPorts( );

        // open port by number (you may need to change this)
        midiIn.openPort( midiPort );
        //midiIn.openPort("IAC Pure Data In");	// by name
        //midiIn.openVirtualPort("ofxMidiIn Input"); // open a virtual port

        // don't ignore sysex, timing, & active sense messages,
        // these are ignored by default
        midiIn.ignoreTypes( false, false, false );

        // add ofApp as a listener and enable direct message handling
        // comment this to use queued message handling
        midiIn.addListener( this );

        // print received messages to the console
        midiIn.setVerbose( settings.getBooleanValue( "verbose" ) );
    }

    if ( screenData.loadFile( "screenManager/screenData.xml" ) ) {
        ofLogNotice( "screenManager/screenData.xml loaded!" );
    } else {
        ofLogError( "ERROR! Unable to Load screenManager/screenData.xml!" );
    }
    if ( sceneData.loadFile( "scenes/sceneData.xml" ) ) {
        ofLogNotice( "scenes/sceneData.xml loaded!" );
    } else {
        ofLogError( "ERROR! Unable to Load scenes/sceneData.xml!" );
    }
    if ( shortcutData.loadFile( "shortcuts/shortcutData.xml" ) ) {
        ofLogNotice( "shortcuts/shortcutData.xml loaded!" );
    } else {
        ofLogError( "ERROR! Unable to Load shortcuts/shortcutData.xml!" );
    }

    
    
    screenManager.setup( settings.getIntValue( "firstMonitorWidth" ), settings.getIntValue( "firstMonitorHeight" ), settings.getIntValue( "contentWidth" ), settings.getIntValue( "contentHeight" ) );

    bezier.init( settings.getIntValue( "contentWidth" ), settings.getIntValue( "contentHeight" ), settings.getIntValue( "maskWidth" ), settings.getIntValue( "maskHeight" ) );

    actualScreen = 0;
    screenCreatePositionAndSize[0] = 10;
    screenCreatePositionAndSize[1] = 10;
    screenCreatePositionAndSize[2] = 800;
    screenCreatePositionAndSize[3] = 600;
    screenCreateGrid[0] = 6;
    screenCreateGrid[1] = 5;
    screenCreateGrid[2] = 10;

    spoutPrefix = "SPOUT: ";
    string syphonPrefix = "SYPHON: ";
    string ndiPrefix = "NDI";
   
    masterSpeed = settings.getFloatValue( "startMasterSpeed" );
    masterSpeedMultiplier = 1;

    loadSlotSettings( );
    loadSceneSettings( );
    loadScreenSettings( );
    loadShortcutSettings( );

    //bezier.init(settings.getIntValue("contentWidth"), settings.getIntValue("contentHeight"), settings.getIntValue("maskWidth"), settings.getIntValue("maskHeight"));

    ofEnableAlphaBlending( );

    if (scenes.size() > 0) {
        startScene( 1 );
    }

    imGuiSpacing = 16.0f;
    //ofEnableBlendMode(OF_BLENDMODE_ALPHA);
    externalList.init( );
    
    // prevent quit on escape
    ofSetEscapeQuitsApp( false );
    
    firstMonitorWidth = settings.getIntValue( "firstMonitorWidth" );
    firstMonitorHeight = settings.getIntValue( "firstMonitorHeight" );

    
    shortcutFlags = (ImGuiInputFlags_RouteGlobal | ImGuiInputFlags_Tooltip );
    
}

//--------------------------------------------------------------

void ofApp::update( ) {

    // quit on shift-ctrl-q
    if ( ofGetKeyPressed(OF_KEY_SHIFT) && ofGetKeyPressed( OF_KEY_ALT ) && ofGetKeyPressed('q') ||
         ofGetKeyPressed(OF_KEY_SHIFT) && ofGetKeyPressed( OF_KEY_ALT ) && ofGetKeyPressed('Q')    
            ) {
            ofExit();
    }

    if ( shortcutState == OE_SHORTCUT_SETKEY) { 
        for (ImGuiKey key = ImGuiKey_Keyboard_BEGIN; key < ImGuiKey_Keyboard_END; key = (ImGuiKey)(key + 1)) { 
            if ( ImGui::IsKeyDown(key) ) {
                ofLogNotice( "key pressed: " +  ofToString ( ImGui::GetKeyName(key) ) );
                tempkey = key;
                shortcutState = OE_SHORTCUT_SETBUTTON; // ToDo: eigetlich zweiter Teil, also OE_SHORTCUT_SETBUTTON
            }                 
        }
    } else if ( shortcutState == OE_SHORTCUT_SETBUTTON ) {
    }

    
    
    
    // get id of pressed Button / Item
    // ofLogNotice( ofToString( ImGui::GetActiveID() ) );

    actualTime = ofGetElapsedTimeMillis( );
    if ( actualTime - cycleTimer > cycledUpdateTimeDelta ) {
        cycledUpdate( );
    }
    screenManager.update( );
    if ( useMidi ) {
        /// queued message handling
        if ( midiIn.hasWaitingMessages( ) ) {
            ofxMidiMessage message;

            // add the latest message to the message queue
            while ( midiIn.getNextMessage( message ) ) {
                midiMessages.push_back( message );
            }

            // remove any old messages if we have too many
            while ( midiMessages.size( ) > maxMessages ) {
                midiMessages.erase( midiMessages.begin( ) );
            }
        }
    }

}


//--------------------------------------------------------------

void ofApp::cycledUpdate( ) {
    // Ubuntu window init hack (need to wait for setting Fullscreen stuff
    if ( windowInited < 5 ) {
        windowInited++;
        if ( windowInited == 4 ) {
            bool fullscr = settings.getBooleanValue( "fullScreen" );
            ofLogNotice( "################# set fullscreen to " + ofToString( fullscr ) );
            if ( fullscr ) {
                ofLogNotice( "set windowshape to " + ofToString( settings.getIntValue( "screenWidth" ) * settings.getIntValue( "numberOfScreens" ) ) + " | " + ofToString( settings.getIntValue( "screenHeight" ) ) );
                ofSetWindowShape( settings.getIntValue( "screenWidth" ) * settings.getIntValue( "numberOfScreens" ), settings.getIntValue( "screenHeight" ) );
                ofSetWindowPosition( 0, 0 );
            }
            // ofSetFullscreen(fullscr);
        }
    }
    //ofLogNotice("cycledUpdate()");
    if (
            masterSpeed != lastMasterSpeed ||
            masterSpeedMultiplier != lastMasterSpeedMultiplier
            ) {
        adjustMasterSpeed( );
    }

    if ( bezier.getActive( ) && screenManager.actualScreen ) {
        screenManager.setMaskPreview( screenManager.actualScreen, bezier.grabMask( maskBlur ) );

    }


}

//--------------------------------------------------------------

void ofApp::draw( ) {

    if ( screenManager.actualScreen <= 0 ) {
        ofClear( 0 );
    } else {
        ofClear( 0 );
        ofSetColor( ofColor( 50, 0, 20 ) );
        ofDrawRectangle( 10, 10, settings.getIntValue( "firstMonitorWidth" ) - 20, settings.getIntValue( "firstMonitorHeight" ) - 20 );
        ofSetColor( 255 );
    }



    // -------------- screens --------------------
    screenManager.draw( );

    
    
    // -------------- Mask Editor --------------------
    if ( bezier.getActive( ) ) {
        bezier.draw( );
        ofSetColor( 255 );
        if ( bezier.getEditBezier( ) ) {
            bezier.drawHelp( );
        }
        ofFill( );
        ofSetColor( 255 );
    }


        // -------------- GUI --------------------

        //required to call this at beginning
        gui.begin( );
        {

            // Make windows transparent, to demonstrate drawing behind them.
            ImGui::PushStyleColor( ImGuiCol_WindowBg, IM_COL32( 200, 200, 200, 128 ) ); // This styles the docked windows

            ImGuiDockNodeFlags dockingFlags = ImGuiDockNodeFlags_PassthruCentralNode; // Make the docking space transparent
            // Fixes imgui to expected behaviour, having a transparent central node in passthru mode.
            // Alternative: Otherwise add in ImGui::DockSpace() [±line 14505] : if (flags & ImGuiDockNodeFlags_PassthruCentralNode) window_flags |= ImGuiWindowFlags_NoBackground;
            ImGui::PushStyleColor( ImGuiCol_ChildBg, IM_COL32( 0, 0, 0, 0 ) );

            //dockingFlags |= ImGuiDockNodeFlags_NoDockingInCentralNode; // Uncomment to always keep an empty "central node" (a visible oF space)
            //dockingFlags |= ImGuiDockNodeFlags_NoTabBar; // Uncomment to disable creating tabs in the main view

            // Define the ofWindow as a docking space
            // Also draws the docked windows at this stage.
            //cout << ImGui::GetMainViewport( )->WorkSize;

            // ToDo: do not draw gui but still react to keystrokes etc.
            ImGui::GetMainViewport( )->WorkSize = ImVec2 ( firstMonitorWidth, firstMonitorHeight );        
            ImGuiID dockNodeID = ImGui::DockSpaceOverViewport( 0, ImGui::GetMainViewport( ), dockingFlags );            

            
            ImGui::PopStyleColor( 2 );


            //ImGui::StyleColorsDark();
            ImGui::PushStyleVar( ImGuiStyleVar_FrameRounding, 6.0f );
            ImGui::PushStyleVar( ImGuiStyleVar_ItemSpacing, ImVec2( 3.0f, 3.0f ) );

            // -------------- GUI Screens Part --------------------
            drawScreensGuiPart( );

            if ( !bezier.getActive( ) ) {

                // -------------- GUI Slots Part --------------------
                drawSlotGuiPart( );

                // -------------- GUI Scenes Part --------------------
                drawScenesGuiPart( );

                // -------------- GUI DMX Part --------------------
                //  drawDMXLightsGuiPart();

                // -------------- GUI Shortcut Management Part --------------------
                drawKeystrokeGuiPart( );
                
                
                // -------------- GUI Shortcut Management Part --------------------
                drawDemoWindowGuiPart ();                       

                // -------------- GUI Master Part --------------------
                drawMasterGuiPart( );

                // -------------- GUI Master Part --------------------
                if ( useFeedbackWindow ) {
//                             drawFeedbackGuiPart();
                }
            }
            ImGui::PopStyleVar( 2 );
        }
    gui.end( );


}


// -----------------------------------

void ofApp::drawScreensGuiPart( ) {

    ImGuiWindowFlags flags = ImGuiWindowFlags_None;
    if (screensWindow_unsaved) {
        flags = ImGuiWindowFlags_UnsavedDocument;
    }     
    ImGui::Begin( "Screens" , 0, flags);
    
    
    setButtonColor( OE_HSV_RED );
    if ( ImGui::Button( "save" ) ) {
        saveScreenSettings( );
    }
    ImGui::PopStyleColor( 3 );

    if ( ImGui::CollapsingHeader( "adjust", ImGuiTreeNodeFlags_DefaultOpen ) ) {
        if ( !screenManager.actualScreen ) {
            // -------------- GUI show Mode Part --------------------
            ImGui::Text( "SHOW MODE!" );
        } else {
            setButtonColor( OE_HSV_BLUE );
            ImGui::Text( "Edit mode" );
            ImGui::SameLine( );
            if ( ImGui::Button( "Go To Show Mode >" ) ) {
                ofLogNotice( "Button clicked" );
                screenManager.actualScreen = 0;
                ofLogNotice( "actual Screen number set to " + ofToString( screenManager.actualScreen ) );
                for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
                    if ( screenManager.getGridState( i + 1 ) ) {
                        screenManager.toggleGrid( i + 1 );
                    }
                }
            }
            ImGui::PopStyleColor( 3 );
        }

        // -------------- GUI screens Part --------------------

        // Color buttons, demonstrate using PushID() to add unique identifier in the ID stack, and changing style.
        for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
            if ( i > 0 ) ImGui::SameLine( );
            ImGui::PushID( i + 12 );
            if ( screenManager.actualScreen == i + 1 ) {
                setHighlightButtonColor( OE_HSV_GREEN, i, 2.0f );
            } else {
                setButtonColor( OE_HSV_GREEN, i, 2.0f );
            }
            string tempstring = "scr " + ofToString( i + 1 );
            if ( ImGui::Button( tempstring.c_str( ) ) ) {
                screenManager.actualScreen = i + 1;
                screenManager.activeScreen = ofToString( screenManager.actualScreen );
                if ( screenManager.actualScreen > screenManager.getScreenCount( ) ) {
                    screenManager.actualScreen = screenManager.getScreenCount( );
                }
                ofLogNotice( "actual Screen number set to " + ofToString( screenManager.actualScreen ) );
            };
            ImGui::PopStyleColor( 3 );
            ImGui::PopID( );
        }
        //for (unsigned int i = 0; i < screenManager.getScreenCount(); i++) {
        //    if (i > 0) ImGui::SameLine();
        //    ImGui::PushID(i+3562);
        //    if (screenManager.getGridState(i + 1)) {
        //        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4) ImColor::HSV(0, 1.0f, 0.5f));
        //    } else {
        //        ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4) ImColor::HSV((i + 6) / 19.0f, 0.6f, 0.4f));
        //    }
        //    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV((i + 6) / 19.0f, 0.7f, 0.5f));
        //    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV((i + 6) / 19.0f, 0.8f, 0.7f));
        //    ImGui::PopStyleColor(3);
        //    ImGui::PopID();
        //}

        setButtonColor( OE_HSV_MAUVE );
        ImGui::SameLine( );
        if ( ImGui::Checkbox( "SoloMode", &actualScreenSoloMode ) ) {
            screenManager.actualScreenSoloMode = actualScreenSoloMode;
        }

        // adjust position and size for active screen
        if ( screenManager.actualScreen ) {
            ImGui::PushItemWidth( 100 );
            ofPoint tempPoint = screenManager.getPosition( screenManager.actualScreen );
            screenCreatePositionAndSize[0] = tempPoint.x;
            screenCreatePositionAndSize[1] = tempPoint.y;
            if ( ImGui::DragInt( "X", &screenCreatePositionAndSize[0] ) ) {
                screenManager.setPosition( screenManager.actualScreen, ofPoint( screenCreatePositionAndSize[0], screenCreatePositionAndSize[1] ) );
                screensWindow_unsaved = true;
            }
            ImGui::SameLine( );
            if ( ImGui::DragInt( "Y", &screenCreatePositionAndSize[1] ) ) {
                screenManager.setPosition( screenManager.actualScreen, ofPoint( screenCreatePositionAndSize[0], screenCreatePositionAndSize[1] ) );
                screensWindow_unsaved = true;
            }
            ImGui::SameLine( );
            ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );


            screenCreatePositionAndSize[2] = screenManager.getScreenWidth( screenManager.actualScreen );
            screenCreatePositionAndSize[3] = screenManager.getScreenHeight( screenManager.actualScreen );
            ImGui::SameLine( );
            if ( ImGui::InputInt( "W", &screenCreatePositionAndSize[2] ) ) {
                screenManager.setScreenWidth( screenManager.actualScreen, screenCreatePositionAndSize[2] );
                screensWindow_unsaved = true;
            }
            ImGui::SameLine( );
            if ( ImGui::InputInt( "H", &screenCreatePositionAndSize[3] ) ) {
                screenManager.setScreenHeight( screenManager.actualScreen, screenCreatePositionAndSize[3] );
                screensWindow_unsaved = true;
            }
            ImGui::PopItemWidth( );
        }
        ImGui::PopStyleColor( 3 );

        if ( screenManager.actualScreen > 0 ) {
            setButtonColor( OE_HSV_MAUVE );
            string temptext = "Screen # " + ofToString( screenManager.actualScreen ) + " to FRONT";

            if ( ImGui::Button( temptext.c_str( ) ) ) {
                if ( screenManager.actualScreen > 0 ) {
                    screenManager.moveScreenToFront( screenManager.actualScreen );
                    screensWindow_unsaved = true;
                }
            }
            ImGui::SameLine( );
            if ( ImGui::Button( "info (to log)" ) ) {
                screenManager.info( );
            }
            ImGui::SameLine( );
            if ( ImGui::Button( "toggle Warping" ) ) {
                screenManager.toggleWarp( screenManager.actualScreen );
                ofLogVerbose( "toggled warp for screen # " + ofToString( screenManager.actualScreen ) );
            }
            ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );
            temptext = "GRID for screen # " + ofToString( screenManager.actualScreen );
            ImGui::Text( temptext.c_str( ) );

            if ( ImGui::Button( "Toggle Grid" ) ) {
                screenManager.toggleGrid( screenManager.actualScreen );
                ofLogVerbose( "toggled grid for screen # " + ofToString( screenManager.actualScreen ) );
            }

            ImGui::SameLine( );
            if ( ImGui::Checkbox( "Grid to controls window", &showGridOnControlScreen ) ) {
                screenManager.showGridOnControlScreenChanged( showGridOnControlScreen );
            }

            ImGui::SameLine( );
            ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );
            ImGui::SameLine( );
            if ( ImGui::Button( "Reset Grid(!)" ) ) {
                if ( screenManager.actualScreen > 0 ) {
                    screenManager.resetWarpGrid( screenManager.actualScreen );
                }
            }

            ImGui::PopStyleColor( 3 );

            if ( screenManager.getGridState( screenManager.actualScreen ) ) {
                setButtonColor( OE_HSV_MAUVE );

                ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );

                ImGui::Columns( 3, "cornerMove", false ); // 3-ways, no border
                ImGui::PushID( 23947823478 );

                // radioButtons do only take int, so a bit of a workaround here
                ImGui::RadioButton( "1", &chosenCornerInt, 1 );
                ImGui::SameLine( );
                ImGui::RadioButton( "2", &chosenCornerInt, 2 );
                ImGui::RadioButton( "4", &chosenCornerInt, 3 );
                ImGui::SameLine( );
                ImGui::RadioButton( "3", &chosenCornerInt, 4 );

                if ( chosenCornerInt != lastChosenCornerInt ) {
                    chosenCorner = (ofOeCorner) chosenCornerInt;
                }
                ImGui::PopID( );
                if ( ImGui::Button( "unset corner" ) ) {
                    chosenCornerInt = 0;
                    chosenCorner = OE_NONE;
                }

                ImGui::NextColumn( );

                if ( chosenCorner != OE_NONE ) {
                    setButtonColor( OE_HSV_MAUVE );
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );
                    ImGui::SameLine( );
                    ImGui::PushButtonRepeat( true );
                    if ( ImGui::ArrowButton( "##up", ImGuiDir_Up ) ) {
                        screenManager.moveCorner( screenManager.actualScreen, chosenCorner, OE_DIRECTION_UP );
                    }

                    if ( ImGui::ArrowButton( "##left", ImGuiDir_Left ) ) {
                        screenManager.moveCorner( screenManager.actualScreen, chosenCorner, OE_DIRECTION_LEFT );
                    }

                    ImGui::SameLine( );
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );
                    ImGui::SameLine( );

                    if ( ImGui::ArrowButton( "##right", ImGuiDir_Right ) ) {
                        screenManager.moveCorner( screenManager.actualScreen, chosenCorner, OE_DIRECTION_RIGHT );
                    }
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );
                    ImGui::SameLine( );
                    if ( ImGui::ArrowButton( "##down", ImGuiDir_Down ) ) {
                        screenManager.moveCorner( screenManager.actualScreen, chosenCorner, OE_DIRECTION_DOWN );
                    }
                    ImGui::PopButtonRepeat( );
                    ImGui::PopStyleColor( 3 );
                } else {
                    setInactiveButtonColor( OE_HSV_MAUVE );
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );

                    ImGui::SameLine( );
                    if ( ImGui::ArrowButton( "##up", ImGuiDir_Up ) ) {
                    }

                    if ( ImGui::ArrowButton( "##left", ImGuiDir_Left ) ) {
                    }
                    ImGui::SameLine( );
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );
                    ImGui::SameLine( );

                    if ( ImGui::ArrowButton( "##right", ImGuiDir_Right ) ) {
                    }
                    ImGui::Dummy( ImVec2( imGuiSpacing, 10.0f ) );
                    ImGui::SameLine( );
                    if ( ImGui::ArrowButton( "##down", ImGuiDir_Down ) ) {
                    }
                    ImGui::PopStyleColor( 3 );

                }

                ImGui::NextColumn( );

                if ( ImGui::Button( "Recalculate" ) ) {
                    screenManager.rearrangeAllPoints( screenManager.actualScreen );
                }
                ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );
                ImGui::SameLine( );
                ImGui::Text( "ALL (!) points" );
                ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );
                ImGui::SameLine( );
                ImGui::Text( "(Perspective Mode)" );




                ImGui::Columns( 1 );
                ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing ) );

                if ( ImGui::Button( "load GRID" ) ) {
                    screenManager.loadWarpSettings( 0 );
                }
                ImGui::SameLine( );

                if ( ImGui::Button( "save GRID" ) ) {
                    screenManager.saveWarpSettings( );
                }

                ImGui::PopStyleColor( 3 );

            }
        }



        if ( ImGui::CollapsingHeader( "Add screen" ) ) {
            setButtonColor( OE_HSV_MAUVE );
            ImGui::InputInt4( "Pos/Size", screenCreatePositionAndSize );
            ImGui::InputInt3( "Grid", screenCreateGrid );
            if ( ImGui::Button( "Add Screen" ) ) {
                screensWindow_unsaved = true;
                addScreen( ofPoint( screenCreatePositionAndSize[0], screenCreatePositionAndSize[1] ), screenCreatePositionAndSize[2], screenCreatePositionAndSize[3], screenCreateGrid[0], screenCreateGrid[1], screenCreateGrid[2] );
            }
            // ToDo: message when nothing happens
            ImGui::PopStyleColor( 3 );
        }
    }
    setButtonColor( OE_HSV_MAUVE );
    if ( ImGui::CollapsingHeader( "remove screen" ) ) {
        ImGui::InputInt( "screenNumber", &removeScreenNumber );
        if ( ImGui::Button( "r!" ) ) {
            if ( removeScreenNumber &&
                 removeScreenNumber<= screenManager.getScreenCount()
                ) {
                removeScreen( removeScreenNumber );
            }
            removeScreenNumber = 0;
            screensWindow_unsaved = true;
        }
    }
    ImGui::PopStyleColor( 3 );


    // -------------- GUI Mask Part --------------------

    if ( ImGui::CollapsingHeader( "Mask" ) ) {

        if ( screenManager.actualScreen > 0 ) {
            setButtonColor( OE_HSV_MAUVE );
            ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing / 2 ) );

            if ( ImGui::Button( "remove Mask" ) ) {
                // set an empty mask
                screenManager.setMask( screenManager.actualScreen, "" );

            }
            ImGui::SameLine( );
            ImGui::PopStyleColor( 3 );
            setButtonColor( OE_HSV_BLUE, 0, 0, 0 );

            if ( ImGui::Button( "load Mask to actual screen >" ) ) {
                ImGui::OpenPopup( "load_pixelmask_popup" );
            }
            if ( ImGui::BeginPopup( "load_pixelmask_popup" ) ) {
                //go through and print out all the paths
                for ( unsigned int i = 0; i < maskDir.size( ); i++ ) {
                    //const char * c = maskDir.getPath(i).c_str();
                    if ( ImGui::MenuItem( maskDir.getPath( i ).c_str( ) ) ) {
                        screenManager.setMask( screenManager.actualScreen, maskDir.getPath( i ) );
                    }
                }
                ImGui::EndPopup( );
            }
            ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing / 2 ) );
            ImGui::PopStyleColor( 3 );
        }
        setButtonColor( OE_HSV_MAUVE );
        ImGui::Columns( 3, "save load Bezier" );
        bool tempMaskEditorActive = bezier.getActive( );
        if ( ImGui::Checkbox( "Mask Editor", &tempMaskEditorActive ) ) {
            bezier.setActive( tempMaskEditorActive );
        }

        ImGui::NextColumn( );
        ImGui::PopStyleColor( 3 );
        setButtonColor( OE_HSV_BLUE, 0, 0, 0 );
        if ( bezier.getActive( ) ) {
            if ( ImGui::Button( "load stored Bezier Mask >" ) ) {
                ImGui::OpenPopup( "load_beziermask_popup" );
            }

            if ( ImGui::BeginPopup( "load_beziermask_popup" ) ) {
                //go through and print out all the paths
                for ( unsigned int i = 0; i < bezierDir.size( ); i++ ) {
                    //const char * c = bezierDir.getPath(i).c_str();
                    if ( ImGui::MenuItem( bezierDir.getPath( i ).c_str( ) ) ) {
                        bezier.loadBezier( bezierDir.getPath( i ) );
                    }
                }
                ImGui::EndPopup( );
            }
        }
        ImGui::NextColumn( );
        ImGui::PopStyleColor( 3 );
        setButtonColor( OE_HSV_RED );


        if ( bezier.getActive( ) ) {
            if ( ImGui::Button( "save actual Bezier Mask (for later)" ) ) {
                ofFileDialogResult saveFileResult = ofSystemSaveDialog( "bezierMask.xml", "Save" );
                if ( saveFileResult.bSuccess ) {
                    bezier.saveBezier( saveFileResult.filePath );
                    ofLogNotice( "bezier mask saved" );
                    listBezierDirectory( );
                }
            }
        }

        ImGui::PopStyleColor( 3 );
        ImGui::Columns( 1 );

        setButtonColor( OE_HSV_MAUVE );
        if ( bezier.getActive( ) ) {
            ImGui::SliderInt( "blur", &maskBlur, 0, 100 );
            ImGui::SameLine( );
            if ( ImGui::Button( "save as Pixel Mask" ) ) {
                bezier.saveMask( maskBlur );
                ofSleepMillis( 500 );
                listMaskDirectory( );
            }
        }
        ImGui::PopStyleColor( 3 );
    }



    if ( ImGui::CollapsingHeader( "Overview.", ImGuiTreeNodeFlags_DefaultOpen ) ) {


        for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
            ImGui::PushID( i + 238476 );
            if ( screenManager.actualScreen == i + 1 ) {
                setHighlightButtonColor( OE_HSV_GREEN, i, 2.0f );
            } else {
                setButtonColor( OE_HSV_GREEN, i, 2.0f );
            }

            string tempstring = "scr " + ofToString( i + 1 );
            ImGui::Button( tempstring.c_str( ) );
            ImGui::SameLine( );
            tempstring = screenManager.getSourceInfo( i + 1 );
            ImGui::Text( tempstring.c_str( ) );
            ImGui::SameLine( );
            tempstring = " -- Target: " + screenManager.getTargetSourceInfo( i + 1 );
            ImGui::Text( tempstring.c_str( ) );

            // in case actual screen runs a video
            if ( screenManager.getSourceType( i + 1 ) == 2 ) {
                float tempVideoSpeed = screenManager.getVideoSpeed( i + 1 );

                if ( ImGui::SliderFloat( "speed", &tempVideoSpeed, 0, 20, "%.3f", 5 ) ) {
                    screenManager.setVideoSpeed( i + 1, tempVideoSpeed, masterSpeed );
                }

                float tempPos = screenManager.getVideoPosition( i + 1 )*100;
                if ( ImGui::SliderFloat( "scrub", &tempPos, 0, 100 ) ) {
                    screenManager.setVideoPosition( i + 1, tempPos / 100 );
                }
            }

            ImGui::Columns( 3, "columns" );
            float tempBrightness = screenManager.getBrightness( i + 1 );
            float tempContrast = screenManager.getContrast( i + 1 );
            float tempSaturation = screenManager.getSaturation( i + 1 );
            if ( ImGui::SliderFloat( "br", &tempBrightness, 0, 3.0 ) ) {
                screenManager.setBrightness( i + 1, tempBrightness );
            }
            ImGui::SameLine( );
            ImGui::PushID( 723684763 );
            if ( ImGui::Button( "->0" ) ) {
                screenManager.setBrightness( i + 1, 1.0 );
            }
            ImGui::PopID( );

            // TODO: brCoSa with arrow butt�ns?
            ImGui::PushID( 7236235763 );
            if ( ImGui::SliderFloat( "co", &tempContrast, 0, 3.0 ) ) {
                screenManager.setContrast( i + 1, tempContrast );
            }
            ImGui::SameLine( );
            if ( ImGui::Button( "->0" ) ) {
                screenManager.setContrast( i + 1, 1.0 );
            }
            ImGui::PopID( );

            // TODO: brCoSa with arrow butt�ns?
            ImGui::PushID( 23763 );

            if ( ImGui::SliderFloat( "sa", &tempSaturation, 0, 3.0 ) ) {
                screenManager.setSaturation( i + 1, tempSaturation );
            }
            ImGui::SameLine( );
            if ( ImGui::Button( "->0" ) ) {
                screenManager.setSaturation( i + 1, 1.0 );
            }
            ImGui::PopID( );
            ImGui::NextColumn( );
            ImGui::PushItemWidth( 90 );

            ofVec3f tempcolor;
            tempcolor = screenManager.getTintColor( i + 1 );
            if ( ImGui::ColorPicker3( "Color", (float*) &tempcolor, ImGuiColorEditFlags_PickerHueWheel | ImGuiColorEditFlags_NoInputs ) ) {
                screenManager.setTintColor( i + 1, tempcolor );
            }
            ImGui::PopItemWidth( );
            ImGui::NextColumn( );
            int tempAlpha = screenManager.getMasterAlpha( i + 1 );
            if ( ImGui::SliderInt( "alpha", &tempAlpha, 0, 255 ) ) {
                screenManager.setMasterAlpha( i + 1, tempAlpha );
            }

            ImGui::Columns( 1 );

            ImGui::Separator( );

            ImGui::PopStyleColor( 3 );
            ImGui::PopID( );

        }

    }



    ImGui::End( );

}


// -----------------------------------

void ofApp::drawSlotGuiPart( ) {
    
    
    ImGuiWindowFlags flags = ImGuiWindowFlags_None;
    if (slotsWindow_unsaved) {
        flags = ImGuiWindowFlags_UnsavedDocument;
    }     
    
    ImGui::Begin( "Slots" , 0, flags);

    //    ImGui::Columns(2, "columns");

    setButtonColor( OE_HSV_RED );

    if ( ImGui::Button( "Add Slot" ) ) {
        addSlot( );
        slotsWindow_unsaved = true;
    }

    ImGui::SameLine( );
    if ( ImGui::Button( "save" ) ) {
        saveSlotSettings( );        
               
    }
    ImGui::PopStyleColor( 3 );

    //    ImGui::NextColumn();
    //    
    //    setRedButtonColor();
    //    if (ImGui::Button("Transition of all (!) slots >")) {      
    //       saveSlotSettings();
    //    }
    //    ImGui::PopStyleColor(3);

    ImGui::Columns( 1 );
    for ( unsigned int i = 0; i < slots.size( ); i++ ) {

        setButtonColor( OE_HSV_MAUVE );
        string addActiveMark = "";
        if ( slots[i]->getIsActive( ) ) {
            addActiveMark = " !";
        }
        string nameofSlot = "Slot " + ofToString( i + 1 ) + addActiveMark;
        slotTypes[i] = slots[i]->type;
        if ( ImGui::CollapsingHeader( nameofSlot.c_str( ), ImGuiTreeNodeFlags_DefaultOpen ) ) {
            ImGui::Columns( 2, "columns" );


            ImGui::PushID( i + 2873 );
            ImGui::RadioButton( "Color", &slotTypes[i], 0 );
            ImGui::SameLine( );
            ImGui::RadioButton( "Image", &slotTypes[i], 1 );
            ImGui::SameLine( );
            ImGui::RadioButton( "Video", &slotTypes[i], 2 );
            ImGui::SameLine( );
            ImGui::RadioButton( "External", &slotTypes[i], 3 );

            if ( slotTypes[i] != (int) slots[i]->getSlotType( ) ) {
                slotsWindow_unsaved = true;

                ofLogNotice( "Slot " + ofToString( i + 1 ) + " set To type " + ofToString( slotTypes[i] ) );
                slots[i]->setSlotType( slotTypes[i] );

                if ( slotTypes[i] == 3 ) {
                    // get External List
                    tExternalList = externalList.list( );
                    externalList.exit( );
                }

            }

            if ( slots[i]->getIsActive( ) ) {
                ImGui::SameLine( );
                ImGui::Button( "-running-" );
            }

            ofFloatColor tempColor;
            ImGui::PopStyleColor( 3 );
            // ------- Color -----

            switch ( slotTypes[i] ) {

                case 0:
                {
                    setButtonColor( OE_HSV_BLUE, 0, 0, 0 );
                    ImGui::PushItemWidth( 150 );
                    slotColors[i].x = slots[i]->color.r;
                    slotColors[i].y = slots[i]->color.g;
                    slotColors[i].z = slots[i]->color.b;
                    if ( ImGui::ColorPicker3( "Color", (float*) &slotColors[i], ImGuiColorEditFlags_PickerHueWheel | ImGuiColorEditFlags_NoInputs ) ) {
                        slotsWindow_unsaved = true;

                        tempColor = ofFloatColor( slotColors[i].x, slotColors[i].y, slotColors[i].z );
                        ofLogNotice( "color changed to " + ofToString( tempColor ) );
                        slots[i]->color = tempColor;
                    }
                    ImGui::PopItemWidth( );
                    ImGui::PopStyleColor( 3 );
                    break;
                }
                    // ------- Image -----                    
                case 1:
                {
                    ImGui::PushItemWidth( 100 );
                    setButtonColor( OE_HSV_BLUE, 0, 0, 0 );

                    ImGui::Button( "load Image >" );
                    if ( ImGui::IsItemHovered( ) ) {
                        ImGui::OpenPopup( "load_image_popup" );
                    }

                    if ( ImGui::BeginPopup( "load_image_popup" ) ) {
                        ImGui::MenuItem( "IMAGES", NULL, false, false );

                        //go through and print out all the paths
                        for ( unsigned int j = 0; j < imageDir.size( ); j++ ) {
                            if ( imageDir.getFile( j ).isDirectory( ) ) {

                                ofDirectory tDir;
                                //files to show
                                tDir.allowExt( "jpg" );
                                tDir.allowExt( "png" );

                                //populate the directory object
                                tDir.listDir( imageDir.getPath( j ) );
                                tDir.sort( );
                                string tImgDirName = imageDir.getName( j );
                                if ( ImGui::BeginMenu( tImgDirName.c_str( ) ) ) {
                                    for ( unsigned int tji = 0; tji < tDir.size( ); tji++ ) {
                                        if ( ImGui::MenuItem( tDir.getName( tji ).c_str( ) ) ) {
                                            slots[i]->setSlotPath( tDir.getPath( tji ) );
                                            //    //ofLogNotice("set path: " + slots[i]->getSlotPath() + " for slot " + ofToString(i));
                                        }
                                    }

                                    ImGui::EndPopup( );
                                }

                            } else {

                                if ( ImGui::MenuItem( imageDir.getName( j ).c_str( ) ) ) {
                                    slots[i]->setSlotPath( imageDir.getPath( j ) );                                    
                                    slotsWindow_unsaved = true;
                                    //ofLogNotice("set path: " + slots[i]->getSlotPath() + " for slot " + ofToString(i));

                                }
                            }
                        }
                        ImGui::EndMenu( );
                    }

                    //                string tempstring = "LOADED: " + ofToString(slots[i]->imagePath);
                    //                ImGui::Text(tempstring.c_str());
                    //ofFbo tempFbo = slots[i]->getContent();

                    ImGui::PopStyleColor( 3 );
                    break;
                }
                    // ------- Video -----     
                case 2:
                {
                    ImGui::PushItemWidth( 100 );
                    setButtonColor( OE_HSV_BLUE, 0, 0, 0 );

                    ImGui::Button( "load Video >" );
                    if ( ImGui::IsItemHovered( ) ) {
                        ImGui::OpenPopup( "load_video_popup" );
                    }

                    if ( ImGui::BeginPopup( "load_video_popup" ) ) {
                        ImGui::MenuItem( "VIDEOS", NULL, false, false );
                        //go through and print out all the paths
                        for ( unsigned int j = 0; j < videoDir.size( ); j++ ) {
                            if ( videoDir.getFile( j ).isDirectory( ) ) {
                                ofDirectory tDir;
                                //files to show
                                tDir.allowExt( "mp4" );
                                tDir.allowExt( "mov" );
                                tDir.allowExt( "mkv" );
                                tDir.allowExt( "m4v" );

                                //populate the directory object
                                tDir.listDir( videoDir.getPath( j ) );
                                tDir.sort( );
                                string tVidDirName = videoDir.getName( j );
                                if ( ImGui::BeginMenu( tVidDirName.c_str( ) ) ) {
                                    for ( unsigned int tjv = 0; tjv < tDir.size( ); tjv++ ) {
                                        if ( ImGui::MenuItem( tDir.getName( tjv ).c_str( ) ) ) {
                                            slots[i]->setSlotPath( tDir.getPath( tjv ) );
                                            //    //ofLogNotice("set path: " + slots[i]->getSlotPath() + " for slot " + ofToString(i));
                                        }
                                    }

                                    ImGui::EndPopup( );
                                }

                            } else {
                                if ( ImGui::MenuItem( videoDir.getName( j ).c_str( ) ) ) {
                                    slots[i]->setSlotPath( videoDir.getPath( j ) );
                                    slotsWindow_unsaved = true;                                    
                                    //ofLogNotice("set path: " + slots[i]->getSlotPath() + " for slot " + ofToString(i));
                                }
                            }
                        }
                        ImGui::EndMenu( );

                    }
                    ImGui::PopItemWidth( );
                    ImGui::PopStyleColor( 3 );
                    setButtonColor( OE_HSV_MAUVE );

                    bool tempState = videoPaused[i];
                    if ( ImGui::Checkbox( "paused", &tempState ) ) {
                        videoPaused[i] = tempState;
                        slots[i]->setVideoPaused( videoPaused[i] );
                    }
                    ImGui::SameLine( );
                    bool tempState2 = videoLooping[i];
                    // set when not clicked ToDo: more elegant rewrite
                    if ( videoLooping[i] ) {
                        slots[i]->setVideoLoopState( OF_LOOP_NORMAL );
                    } else {
                        slots[i]->setVideoLoopState( OF_LOOP_NONE );
                    }
                    // set, when clicked
                    if ( ImGui::Checkbox( "loop", &tempState2 ) ) {
                        videoLooping[i] = tempState2;
                        if ( videoLooping[i] ) {
                            slots[i]->setVideoLoopState( OF_LOOP_NORMAL );
                        } else {
                            slots[i]->setVideoLoopState( OF_LOOP_NONE );
                        }
                    }
                    ImGui::SameLine( );
                    if ( ImGui::Button( "speed to 1" ) ) {
                        videoSpeed[i] = 1.0;
                        slots[i]->setVideoSpeed( videoSpeed[i] );
                    }
                    ImGui::SameLine( );
                    // TODO: check if there is a flash, find better approach in restartVideo() if yes
                    if ( ImGui::Button( "rewind" ) ) {
                        slots[i]->restartVideo( );
                    }
                    ImGui::PushItemWidth( 350 );

                    float tempPos = slots[i]->getVideoPosition( )*100;
                    if ( ImGui::SliderFloat( "scrub", &tempPos, 0, 100 ) ) {
                        slots[i]->setVideoPosition( tempPos );
                    }
                    // set when not clicked ToDo: more elegant rewrite
                    slots[i]->setVideoSpeed( videoSpeed[i] );
                    // set, when clicked                
                    if ( ImGui::SliderFloat( "speed", &videoSpeed[i], 0, 20, "%.3f", 5 ) ) {
                        slots[i]->setVideoSpeed( videoSpeed[i] );
                    }
                    ImGui::PopItemWidth( );
                    ImGui::PopStyleColor( 3 );
                    // ImGui::Image()
                    break;
                }
                case 3:
                {
                    // ------- External -----    

                    setButtonColor( OE_HSV_BLUE, 0, 0, 0 );
                    if ( tExternalList.size( ) ) {
                        ImGui::Button( "load External >" );
                        if ( ImGui::IsItemHovered( ) ) {
                            ImGui::OpenPopup( "load_external_popup" );
                        }

                        if ( ImGui::BeginPopup( "load_external_popup" ) ) {
                            for ( unsigned int j = 0; j < tExternalList.size( ); j++ ) {
                                if ( ImGui::MenuItem( tExternalList[j].c_str( ) ) ) {
                                    slots[i]->setSlotPath( tExternalList[j] );
                                    //ofLogNotice("set path: " + slots[i]->getSlotPath() + " for slot " + ofToString(i) );
                                }
                            }
                            ImGui::EndPopup( );

                        }
                    } else {
                        ImGui::Text( "no external source" );
                    }
                    ImGui::PopStyleColor( 3 );
                    break;
                }
            }

            setButtonColor( OE_HSV_MAUVE );

            ImGui::PushItemWidth( 350 );
            int tempFadeTime = slots[i]->fadeTimeForSlot;
            if ( ImGui::SliderInt( "fade time", &tempFadeTime, 0, 3600 ) ) {
                slots[i]->fadeTimeForSlot = tempFadeTime;
            }

            int tempAlpha = slots[i]->targetAlpha;
            if ( ImGui::SliderInt( "alpha", &tempAlpha, 0, 255 ) ) {
                slots[i]->targetAlpha = tempAlpha;
            }
            ImGui::PopItemWidth( );

            ImGui::PushItemWidth( 90 );
            float tempBrightness = slots[i] ->brightness;
            float tempContrast = slots[i]->contrast;
            float tempSaturation = slots[i]->saturation;
            if ( ImGui::SliderFloat( "br", &tempBrightness, 0, 3.0 ) ) {
                slots[i]->brightness = tempBrightness;
            }
            ImGui::SameLine( );
            if ( ImGui::SliderFloat( "co", &tempContrast, 0, 3.0 ) ) {
                slots[i]->contrast = tempContrast;
            }
            ImGui::SameLine( );
            if ( ImGui::SliderFloat( "sa", &tempSaturation, 0, 3.0 ) ) {
                slots[i]->saturation = tempSaturation;
            }
            ImGui::SameLine( );
            if ( ImGui::Button( "->0" ) ) {
                slots[i]->brightness = 1.0;
                slots[i]->contrast = 1.0;
                slots[i]->saturation = 1.0;
            }



            ImGui::PopItemWidth( );

            string tempstring = "(set: " + ofToString( slots[i]->getInfo( ) ) + ")";
            ImGui::Text( tempstring.c_str( ) );

            ImGui::PopID( );
            ImGui::NextColumn( );
            ImGui::PushID( 1000 + i );



            ImGui::Text( tempstring.c_str( ) );

            // change To button
            ImGui::PushStyleVar( ImGuiStyleVar_FrameRounding, 12.0f );

            /*            if (slots[i]->targetAlpha < 255) {
                            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4) ImColor::HSV(1.0f, 0.8f, 0.8f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV(1.0f, 0.8f, 0.8f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV(1.0f, 0.8f, 0.8f));
                            ImGui::Button("<-");
                            ImGui::SameLine();
                            ImGui::TextColored(ImVec4(0.9, 0.2, 0, 1), "- Transition!");
                        } else {
                            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4) ImColor::HSV(0.4f, 0.6f, 0.6f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV(0.4f, 0.7f, 1.0f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV(0.4f, 0.8f, 0.8f));
                            if (ImGui::Button("<-")) {
                                slots[i]->startTransitionForSlot();
                            }
                        }      */

            for ( unsigned int screeni = 0; screeni < screenManager.getScreenCount( ); screeni++ ) {
                //if (i > 0) ImGui::SameLine();
                string slscid = "slot-" + ofToString(i+1) + "-screen-" + ofToString(screeni+1);
                ImGui::PushID( slscid.c_str() );
                if ( screenManager.actualScreen == screeni + 1 ) {
                    setHighlightButtonColor( OE_HSV_GREEN, (int) screeni, 2.0f );
                } else {
                    setButtonColor( OE_HSV_GREEN, screeni, 2.0f , true);
                }

                string tempstring = "to screen " + ofToString( screeni + 1 );

                if (shortcutList[ ofToString(slscid) +  "-" + tempstring ]) {
                    ImGui::SetNextItemShortcut( shortcutList[ ofToString(slscid) +  "-" + tempstring ], shortcutFlags );
                }

                if ( ImGui::Button( tempstring.c_str( ) ) ) {
                   if ( !makeItAShortcut( slscid,  tempstring ) ) {
                        // remove path in case we have a Color (just to be correct)
                        if ( slotTypes[i] == 0 ) {
                            //ofLogNotice("reset Slot Path (as we have a color)");
                            slots[i]->setSlotPath( "" );
                        }

                        tempColor = ofFloatColor( slotColors[i].x, slotColors[i].y, slotColors[i].z );

                        //ofLogVerbose("ofApp::screenManager.setTargetSource for sreen " + ofToString(screeni + 1) + ", type: " + ofToString(slotTypes[i]) + ", path: " + slots[i]->getTargetSlotPath() );
                        screenManager.setTargetSource( screeni + 1, slotTypes[i], slots[i]->getSlotPath( ), tempColor, tempFadeTime, slots[i]->getVideoLoopState( ), slots[i]->getVideoSpeed( ), masterSpeed, slots[i]->targetAlpha );
                   }
                };
                ImGui::PopStyleColor( 3 );
                ImGui::PopID( );


                //                bool nurso;
                //                ImGui::SameLine();
                //                if (ImGui::Checkbox("pre", &nurso) )  {
                //                    
                //                }

            }

            ImGui::PopStyleVar( 1 );

            ImGui::PopID( );
            ImGui::Columns( 1 );

        } else {
            ImGui::SameLine( );
            string contentOfSlot = "";

            if ( slots[i] ) {

                contentOfSlot += " | " + slots[i]->getInfo( );
            }

            ImGui::TextColored( ImVec4( 0.9, 0.9, 0.9, 1 ), contentOfSlot.c_str( ) );
        }
        ImGui::Separator( );
        ImGui::PopStyleColor( 3 );
    }

    ImGui::End( );

}



// -----------------------------------

void ofApp::drawScenesGuiPart( ) {
    setButtonColor( OE_HSV_RED );
    ImGui::Begin( "Scenes" );
    if ( ImGui::Button( "New Scene" ) ) {
        addScene( );
    }
    ImGui::SameLine( );

    if ( ImGui::Button( "end Edit" ) ) {
        actualScene = 0;
    }


    // Color buttons, demonstrate using PushID() to add unique identifier in the ID stack, and changing style.
    for ( unsigned int i = 0; i < scenes.size( ); i++ ) {
        string id = "Scenes-" + ofToString( i );
        
        ImGui::PushID( id.c_str() );

        if ( actualScene == i + 1 ) {
            setHighlightButtonColor( OE_HSV_BLUE );
        } else {
            setButtonColor( OE_HSV_BLUE );
        }


        if ( ImGui::Button( "edit" ) ) {
            actualScene = i + 1;
            if ( actualScene > scenes.size( ) ) {
                actualScene = scenes.size( );
            }
        }
        ImGui::SameLine( );

        ImGui::PopStyleColor( 3 );
        //        if (actualScene == i + 1) {
        //            setHighlightButtonColor(OE_HSV_GREEN, i);
        //        } else {
        setButtonColor( OE_HSV_GREEN, (int) i );
        //        }

        if ( ImGui::Button( "just prep. slots" ) ) {
            loadSlotsForScene( i + 1 );
        }
        ImGui::SameLine( );
        ImGui::PopStyleColor( 3 );

        //        if (actualScene == i + 1) {
        //            setHighlightButtonColor(OE_HSV_ORANGE);
        //        } else {
        setHighlightButtonColor( OE_HSV_BLUE, true );
        //        }
        
        string tname = "GO!";
        if (shortcutList[ ofToString(id) +  "-" + tname ]) {
            ImGui::SetNextItemShortcut( shortcutList[ ofToString(id) +  "-" + tname ], shortcutFlags );
        }
        if ( ImGui::Button( tname.c_str() ) ) {
           if ( !makeItAShortcut( id,  tname ) ) {
                startScene( i + 1 );
           }
        }

        ImGui::PopStyleColor( 3 );


        setButtonColor( OE_HSV_MAUVE );

        if ( actualScene == i + 1 ) {
            ImGui::SameLine( );
            ImGui::PushItemWidth( 100 );
            if ( ImGui::InputInt( "", &tempSceneNumber[i] ) ) {
                scenes[i]->setNumber( tempSceneNumber[i] );
            }
            ImGui::PopItemWidth( );
        } else {
            ImGui::SameLine( );
            string tempNum = ofToString( scenes[i]->getNumber( ) );
            ImGui::Text( tempNum.c_str( ) );
            ImGui::SameLine( );
            string nameofScene = scenes[i]->getName( );
            ImGui::Text( nameofScene.c_str( ) );
        }
        if ( actualScene == i + 1 ) {
            ImGui::SameLine( );
            string temp = scenes[i]->getName( );
            static char buf[32] = "new";
            strcpy( buf, temp.c_str( ) );
            ImGui::PushItemWidth( 250 );

            if ( ImGui::InputText( "name", buf, IM_ARRAYSIZE( buf ) ) ) {
                scenes[i]->setName( ofToString( buf ) );
            }
            ImGui::PopItemWidth( );

            ImGui::SameLine( );
            if ( ImGui::Button( "save" ) ) {

                saveSceneSettings( actualScene );
            }
        }


        ImGui::PopStyleColor( 3 );
        ImGui::PopID( );
    }
    ImGui::TextColored( ImVec4( 0.9, 0.2, 0, 1 ), sceneMessage.c_str( ) );

    ImGui::PopStyleColor( 3 );

    ImGui::End( );

}

// -----------------------------------

void ofApp::drawDemoWindowGuiPart() {
        ImGui::Begin( "ImGui Demo Window");
    if ( ImGui::CollapsingHeader( "Demo Window (Degbug, Possibilitites)" ) ) {
        setButtonColor( OE_HSV_MAUVE );
        ImGui::Text( "Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO( ).Framerate, ImGui::GetIO( ).Framerate );
        //
        if ( ImGui::Button( "Open Demo Window" ) ) {
            show_test_window = !show_test_window;
        }
        if ( show_test_window ) {

            ImGui::SetNextWindowPos( ofVec2f( 650, 20 ), ImGuiCond_FirstUseEver );
            ImGui::ShowDemoWindow( &show_test_window );
        } //required to call this at end
        ImGui::PopStyleColor( 3 );

    }    
    
    ImGui::End( );
        

}

// -----------------------------------

void ofApp::drawKeystrokeGuiPart( ) {
    ImGuiWindowFlags flags = ImGuiWindowFlags_None;
    if (shortcutWindow_unsaved) {
        flags = ImGuiWindowFlags_UnsavedDocument;
    } 
    ImGui::Begin( "Keyboard Shortcuts", 0, flags);
    
    if ( ImGui::CollapsingHeader( "Key Shortcuts" ) ) {
        setButtonColor( OE_HSV_RED );
        if ( ImGui::Button( "save" ) ) {
            saveShortcutSettings( );
        }
        ImGui::PopStyleColor( 3 );

        setButtonColor( OE_HSV_MAUVE );

        if ( ImGui::Button( "Set Shortcut" ) ) {
            listenToKeyShortCut( );
        }
        if ( shortcutState != OE_SHORTCUT_NONE ) {
            ImGui::ProgressBar(-1.0f * (float)ImGui::GetTime(), ImVec2(0.0f, 0.0f), "Setting Shortcut..");    
        }

        if ( shortcutState == OE_SHORTCUT_SETKEY ) {
            string tstring = "Chosee desired key, please" ;
            ImGui::Text(tstring.c_str());
        }
        if ( shortcutState == OE_SHORTCUT_SETBUTTON ) {
            string tstring = "Chosen key: " + ofToString( ImGui::GetKeyName(tempkey) );
            ImGui::Text(tstring.c_str());
            ImGui::Text( "click on desired Button to bind to chosen key." );
        }

        for (auto it = shortcutList.cbegin(), next_it = it; it != shortcutList.cend(); it = next_it)
        {
            ++next_it;
            if ( it->second ) {
            
                ImGui::Text( ImGui::GetKeyName(it->second) );
                ImGui::SameLine();
                ImGui::Text(": ");
                ImGui::SameLine();
                ImGui::Text( it->first.c_str() );
                ImGui::SameLine();
                
                string tt = "delete " + it->first;
                ImGui::PushID( tt.c_str() );
                if (ImGui::Button( "x" ) ) {
                    ImGui::OpenPopup( tt.c_str() );
                }
                if ( ImGui::BeginPopup( tt.c_str()  ) ) {

                    ImGui::Text("Delete Keystroke?");
                    if (ImGui::Button( "y" ) ) {
                        ImGui::CloseCurrentPopup();
                       shortcutList.erase(it);
                       shortcutWindow_unsaved = true;
                    } 
                    ImGui::SameLine();
                    if (ImGui::Button( "n" ) ) {
                        ImGui::CloseCurrentPopup();

                    }
                   ImGui::EndPopup( );
                }
                ImGui::PopID();
                
            
            }

        }        
                ImGui::PopStyleColor( 3 );

        
//        for (auto const& x : shortcutList)
//        {
//            if ( x.second ) {
//                ImGui::Text( ImGui::GetKeyName(x.second) );
//                ImGui::SameLine();
//                ImGui::Text(": ");
//                ImGui::SameLine();
//                ImGui::Text( x.first.c_str() );
//                ImGui::SameLine();
//                
//                string tt = "delete " + x.first;
//                if (ImGui::Button( "x" ) ) {
//                    ImGui::OpenPopup( tt.c_str() );
//                }
//                if ( ImGui::BeginPopup( tt.c_str()  ) ) {
//
//                    ImGui::Text("Delete Keystroke?");
//                    if (ImGui::Button( "y" ) ) {
//                        ImGui::CloseCurrentPopup();
//                       // shortcutList.erase(x);
//                    } 
//                    ImGui::SameLine();
//                    if (ImGui::Button( "n" ) ) {
//                        ImGui::CloseCurrentPopup();
//
//                    }
//                   ImGui::EndPopup( );
//                }
//
//
//
//            }
//        }

    
    }
    
    
    ImGui::End( );

}


// -----------------------------------

void ofApp::listenToKeyShortCut( ) {
    shortcutState = OE_SHORTCUT_SETKEY;
}

// -----------------------------------

bool ofApp::makeItAShortcut( string id, string name ) {
    if ( shortcutState == OE_SHORTCUT_SETBUTTON ) {
        
//    for (auto it = shortcutList.cbegin(); it != shortcutList.cend(); )
//    {
//        if (it->second == tempkey )
//        {
//            // supported in C++11
//            it = shortcutList.erase(it);
//        }
//        else {
//            ++it;
//        }
//    }        
//        
        ofLogNotice( " - DEBUG: '" + ofToString( ImGui::GetKeyName(tempkey) ) ); 
        for (auto it = shortcutList.cbegin(), next_it = it; it != shortcutList.cend(); it = next_it)
        {
        ofLogNotice( " - ONE check: '" + ofToString( it->second ) ); 
          ++next_it;
          if (it->second == tempkey)
          {
        ofLogNotice( " ###DELETE###: '"  + ofToString( it->second ) ); 
            shortcutList.erase(it);
          }
        }        
        shortcutWindow_unsaved = true;
        ofLogNotice( "Bind '" + ofToString( ImGui::GetKeyName(tempkey) ) + "' to ID: " + ofToString(id) +  ", Button: " + name ); 
        shortcutList[ ofToString(id) +  "-" + name ] = tempkey;

        shortcutState = OE_SHORTCUT_NONE;
        return true;       
    } else {
        return false;
    }
}

// -----------------------------------

void ofApp::keyPressed( int key ) {
      //  ofLogNotice( ofToString( tempPressedKeys.size() ) );
    
    
}
// -----------------------------------

void ofApp::keyReleased( int key ) {
    if(key == OF_KEY_ESC) {
        drawGui = !drawGui;
    }
}

// -----------------------------------

void ofApp::drawMasterGuiPart( ) {
    ImGui::Begin( "Master" );

    ImGui::PushStyleVar( ImGuiStyleVar_ItemSpacing, ImVec2( 18.0f, 12.0f ) );


    //    if (ImGui::SliderFloat("Speed", &masterSpeed, -10, 10)) {
    //        // master speed changed
    //        adjustMasterSpeed(masterSpeed);
    //    }

    ImGui::Columns( 3 );
    ImGui::Text( "Speed" );

    setButtonColor( OE_HSV_MAUVE );

    if ( ImGui::Button( "to 1" ) ) {
        masterSpeed = 1.0f;
        masterSpeedMultiplier = 1.0f;
    }

    ImGui::PopStyleColor( 3 );

//    ImGui::SetColumnWidth( -1, 70.0 );
    ImGui::PushID( "speedMaster" );

    setSliderColor( OE_HSV_RED );

    if ( ImGui::VSliderFloat( "", ImVec2( 25, 460 ), &masterSpeed, -1.0f, 1.0f, "" ) ) {
        //adjustMasterSpeed(masterSpeed);
    }
    if ( ImGui::IsItemActive( ) || ImGui::IsItemHovered( ) )
        ImGui::SetTooltip( "%.3f", masterSpeed );

    ImGui::PopStyleColor( 5 );
    
    setButtonColor( OE_HSV_MAUVE );
    if ( ImGui::Button( "to 0" ) ) {
        masterSpeed = 0.00000001f;
    }
    
    ImGui::PopStyleColor( 3 );
    

    ImGui::Text( ofToString( masterSpeed ).c_str( ) );
    ImGui::PopID( );
    ImGui::NextColumn( );
    ImGui::PushID( "speedMultiplier" );

    //    ImGui::SetColumnWidth(-1, 95.0);
    ImGui::Text( "Multiplier" );
    ImGui::Dummy( ImVec2( imGuiSpacing, imGuiSpacing * 3 ) );

    setSliderColor( OE_HSV_MAUVE );

    ImGui::VSliderFloat( "", ImVec2( 25, 460 ), &masterSpeedMultiplier, 1, 10.0f, "" );
    if ( ImGui::IsItemActive( ) || ImGui::IsItemHovered( ) )
        ImGui::SetTooltip( "%.3f", masterSpeedMultiplier );
    ImGui::PopStyleColor( 5 );

    ImGui::Text( ofToString( masterSpeedMultiplier ).c_str( ) );
    ImGui::PopID( );
    ImGui::NextColumn( );


    ImGui::Text( "Transparency" );

    setButtonColor( OE_HSV_PETROL );

//    ImGui::SameLine( );

    if ( ImGui::Button( "FULL" ) ) {
        masterAlpha = 1.0f;
    }
    ImGui::PopStyleColor( 3 );

    setSliderColor( OE_HSV_PETROL );

    ImGui::VSliderFloat( "Alpha", ImVec2( 25, 460 ), &masterAlpha, 0, 1.0f, "" );

    if ( ImGui::IsItemActive( ) || ImGui::IsItemHovered( ) )
        ImGui::SetTooltip( "%.3f", masterAlpha );

    ImGui::PopStyleColor( 5 );

    
    setButtonColor( OE_HSV_PETROL );
    
    if ( ImGui::Button( "Black" ) ) {
        masterAlpha = 0.00000001f;
    }

    ImGui::PopStyleColor( 3 );



    ImGui::Columns( 1 );
    ImGui::PopStyleVar( 1 );
    ImGui::End( );

}


// -----------------------------------

void ofApp::adjustMasterSpeed( ) {
    ofLogNotice( "adjust Master Speed to " + ofToString( masterSpeed ) );

    for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
        screenManager.setVideoSpeed( i + 1, screenManager.getVideoSpeed( i + 1 ), masterSpeed * masterSpeedMultiplier );
    }
    lastMasterSpeed = masterSpeed;
    lastMasterSpeedMultiplier = masterSpeedMultiplier;
}


// -----------------------------------

void ofApp::addScreen( ofPoint position, int width, int height, int pointsX, int pointsY, int pixelsPerGridDivision ) {
    ofFbo tempFbo;
    tempFbo.allocate( width, height, GL_RGB );
    screenManager.addScreen( position, width, height, pointsX, pointsY, pixelsPerGridDivision );

    maskPaths.push_back( settings.getStringValue( "deafultMask" ) );

    slotNumberForScreen.push_back( 0 );
    //    targetSlotNumberForScreen.push_back(0);
    //    targetSlotNumberForScreenUnApproved.push_back(0);
    //    targetSlotNumberAlphaForScreen.push_back(0);
    //    fadeTimeForScreen.push_back(500);
    //    fadeTimerForScreen.push_back(0);

}

// -----------------------------------

void ofApp::removeScreen( int tempScreenNumber ) {

    screenManager.removeScreen( tempScreenNumber );
    // TODO: check position. Remove screen on the middle, screen(s?) after can't be moved any more

    maskPaths.erase( maskPaths.begin( ) + (tempScreenNumber - 1) );
    slotNumberForScreen.erase( slotNumberForScreen.begin( ) + (tempScreenNumber - 1) );
    //    targetSlotNumberForScreen.erase(targetSlotNumberForScreen.begin() + (tempScreenNumber - 1));
    //    targetSlotNumberForScreenUnApproved.erase(targetSlotNumberForScreenUnApproved.begin() + (tempScreenNumber - 1));
    //    targetSlotNumberAlphaForScreen.erase(targetSlotNumberAlphaForScreen.begin() + (tempScreenNumber - 1));
    //    fadeTimeForScreen.erase(fadeTimeForScreen.begin() + (tempScreenNumber - 1));
    //
}




// -----------------------------------

void ofApp::addSlot( ) {

    ofLogNotice( "addSlot()" );
    oeSlot* slot = new oeSlot( );
    slotTypes.push_back( 1 );
    slot->init( );
    slot->targetAlpha = 255;
    slot->fadeTimeForSlot = 1200;
    slots.push_back( slot );
    slotColors.push_back( startSlotColor );
    targetSlotColors.push_back( startSlotColor );
    videoPaused.push_back( false );
    videoLooping.push_back( true );
    videoSpeed.push_back( 1.0 );
    targetVideoPaused.push_back( false );
    targetVideoLooping.push_back( true );
    targetVideoSpeed.push_back( 1.0 );

    ofLogNotice( "slotTypes " + ofToString( slotTypes.size( ) ) );
    ofLogNotice( "slotColors " + ofToString( slotColors.size( ) ) );
    ofLogNotice( "slots " + ofToString( slots.size( ) ) );
    ofLogNotice( "targetSlotColors " + ofToString( targetSlotColors.size( ) ) );
    ofLogNotice( "videoPaused " + ofToString( videoPaused.size( ) ) );
    ofLogNotice( "videoSpeed " + ofToString( videoSpeed.size( ) ) );
    ofLogNotice( "videoLooping " + ofToString( videoLooping.size( ) ) );
    ofLogNotice( "targetVideoPaused " + ofToString( targetVideoPaused.size( ) ) );
    ofLogNotice( "targetVideoSpeed " + ofToString( targetVideoSpeed.size( ) ) );


}

// -----------------------------------

void ofApp::addScene( ) {

    oeScene* scene = new oeScene( );
    scene->addScene( slotNumberForScreen );
    scene->setNumber( sceneCounter );
    scene->setName( "new" );
    scenes.push_back( scene );

    // set the local variables for use in ImGUI 
    //( &tempSceneNumber etc. Dont want to do this with getters/setters )
    tempSceneNumber.push_back( sceneCounter );
    tempSceneName.push_back( scene->getName( ) );

    sceneCounter += 10;
}


// -----------------------------------

void ofApp::loadSlotsForScene( int sceneNumber ) {
    ofLogVerbose( "start Scene Number " + ofToString( sceneNumber ) + ": try to laod slot settings..." );
    loadSlotSettings( sceneNumber );
}

// -----------------------------------

void ofApp::startScene( int sceneNumber ) {
    loadSlotsForScene( sceneNumber );
    ofLogVerbose( "try to load screen to slot settings..." );
    loadScreensForScene( sceneNumber );

    //    loadScreenToSlotSettings(sceneNumber);
    //    ofLogNotice("Vector tempScreenToSlot for sceneNumber " + ofToString(sceneNumber));
    //    vector <int> tempScreenToSlot = scenes[sceneNumber - 1]->getSlots();
    //    for (unsigned int i = 0; i < tempScreenToSlot.size(); i++) {
    //        targetSlotNumberForScreen[i] = tempScreenToSlot[i];
    //        //fadeTimeForScreen[i] = 500;
    //        ofLogNotice("startTransitionForScreen " + ofToString(i + 1));
    //        startTransitionForScreen(i + 1);
    //    }
}


// -----------------------------------

void ofApp::loadSceneSettings( ) {
    int numberOfSceneNodes;
    sceneData.pushTag( "SETTINGS", 0 );
    numberOfSceneNodes = sceneData.getNumTags( "scene" );
    ofLogNotice( ofToString( numberOfSceneNodes ) + " scene nodes found" );
    for ( int i = 0; i < numberOfSceneNodes; i++ ) {
        addScene( );
        scenes[i]->setName( sceneData.getValue( "scene:name", "", i ) );
        scenes[i]->setNumber( sceneData.getValue( "scene:number", 0, i ) );
        vector <int> tempTargetScreenToSlotValues;
        numberOfSceneNodes = sceneData.getNumTags( "scene" );
        sceneData.pushTag( "scene", i );
        for ( int j = 0; j < sceneData.getNumTags( "screen" ); j++ ) {
            tempTargetScreenToSlotValues.push_back( sceneData.getValue( "screen:slot", 0, j ) );
        }
        scenes[i]->setSlots( tempTargetScreenToSlotValues );
        sceneData.popTag( );
    }
    sceneData.popTag( );
}

// -----------------------------------

void ofApp::saveSceneSettings( int sceneNumber ) {
    //sceneData.clear();
    //sceneData.addTag("SETTINGS");

    if ( sceneData.pushTag( "SETTINGS", 0 ) ) {

        ofLogNotice( ofToString( sceneData.getNumTags( "scene" ) ) );
        if ( sceneData.getNumTags( "scene" ) >= sceneNumber ) {
            sceneData.clearTagContents( "scene", sceneNumber - 1 );
            prepareInnerSceneSettingsForSave( sceneNumber - 1, sceneNumber );
            ofLogNotice( "scene: " + ofToString( sceneNumber ) + " (index " + ofToString( sceneNumber - 1 ) + "): changed in XML" );
        } else if ( sceneData.getNumTags( "scene" ) == sceneNumber - 1 ) {
            int tagNum = sceneData.addTag( "scene" );
            prepareInnerSceneSettingsForSave( tagNum, sceneNumber );
            ofLogNotice( "scene: " + ofToString( sceneNumber ) + " (index " + ofToString( sceneNumber - 1 ) + "): added to XML" );
        } else {
            addFeedback( "please add scene number " + ofToString( sceneData.getNumTags( "scene" ) + 1 ) + " next", OF_LOG_NOTICE, true );
        }
        sceneData.popTag( );

#if OF_VERSION_MINOR>11
        sceneData.save( );
#else
        sceneData.saveFile( );
#endif
    }
}

// -----------------------------------

void ofApp::prepareInnerSceneSettingsForSave( int tagNumber, int sceneNumber ) {
    // set base stuff
    sceneData.setValue( "scene:name", scenes[sceneNumber - 1]->getName( ), tagNumber );
    sceneData.setValue( "scene:number", scenes[sceneNumber - 1]->getNumber( ), tagNumber );
    saveSlotSettings( sceneNumber );
    saveSceneScreenSettings( sceneNumber );
    //saveScreenToSlotSettings(sceneNumber);
}

// -----------------------------------

void ofApp::addFeedback( string _message, ofLogLevel _level ) {
    ofLog( _level, _message );
}
// -----------------------------------

void ofApp::addFeedback( string _message, ofLogLevel _level, bool _showFlashMessage ) {
    ofLog( _level, _message );
    if ( _showFlashMessage ) {
        flashMessages.add( _message, 6, _level );
    }
}

void ofApp::listDirectories( ) {

    //files to show
    videoDir.allowExt( "mp4" );
    videoDir.allowExt( "m4v" );
    videoDir.allowExt( "mov" );
    videoDir.allowExt( "mkv" );
    // also list directories for ordering
    videoDir.allowExt( "" );

    //populate the directory object
    videoDir.listDir( "videos/" );
    videoDir.sort( );

    //files to show
    imageDir.allowExt( "jpg" );
    imageDir.allowExt( "png" );
    // also list directories for ordering
    imageDir.allowExt( "" );
    //populate the directory object
    imageDir.listDir( "images/" );
    imageDir.sort( );

    listMaskDirectory( );
    listBezierDirectory( );
}

//--------------------------------------------------------------

void ofApp::listMaskDirectory( ) {
    //files to show
    maskDir.allowExt( "jpg" );
    //populate the directory object
    maskDir.listDir( "masks/" );
    maskDir.sort( );
}

//--------------------------------------------------------------

void ofApp::listBezierDirectory( ) {
    //files to show
    bezierDir.allowExt( "xml" );
    //populate the directory object
    bezierDir.listDir( "beziers/" );
    bezierDir.sort( );
}



// -----------------------------------

void ofApp::saveScreenSettings( ) {
    screenData.clear( );
    screenData.addTag( "SETTINGS" );

    if ( screenData.pushTag( "SETTINGS" ) ) {

        for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
            ofLogNotice( "screen: " + ofToString( i + 1 ) + " (index " + ofToString( i ) + "): added to XML" );

            int tagNum = screenData.addTag( "screen" );
            screenData.setValue( "screen:x", screenManager.screenPosition[i].x, tagNum );
            screenData.setValue( "screen:y", screenManager.screenPosition[i].y, tagNum );
            screenData.setValue( "screen:width", screenManager.getScreenWidth( i + 1 ), tagNum );
            screenData.setValue( "screen:height", screenManager.getScreenHeight( i + 1 ), tagNum );
            screenData.setValue( "screen:pointsX", screenManager.getScreenNumXPoints( i + 1 ), tagNum );
            screenData.setValue( "screen:pointsY", screenManager.getScreenNumYPoints( i + 1 ), tagNum );
            screenData.setValue( "screen:pixelsPerGridDivision", screenManager.getScreenWarpGridResolution( i + 1 ), tagNum );
            screenData.setValue( "screen:mask", screenManager.getMaskPath( i + 1 ), tagNum );
        }
    }
    screenData.popTag( );
#if OF_VERSION_MINOR>11
    screenData.save( );
#else
    screenData.saveFile( );
#endif

    screensWindow_unsaved = false; 
}



// -----------------------------------

void ofApp::saveShortcutSettings( ) {
    shortcutData.clear();
    shortcutData.addTag( "SETTINGS" );
    
    if ( shortcutData.pushTag( "SETTINGS" ) ) {
    
     for (auto const& x : shortcutList)
    {
        if ( x.second ) {
            int tagNum = shortcutData.addTag( "shortcut" );
            shortcutData.setValue( "shortcut:key", x.second, tagNum );
            shortcutData.setValue( "shortcut:button", x.first, tagNum );
        }   
     }
    shortcutData.popTag( );
#if OF_VERSION_MINOR>11
    shortcutData.save( );
#else
    shortcutData.saveFile( );
#endif
    }
    
            shortcutWindow_unsaved = false;
    
}
    

//                ImGui::Text( ImGui::GetKeyName(x.second) );
//                ImGui::SameLine();
//                ImGui::Text(": ");
//                ImGui::SameLine();
//                ImGui::Text( x.first.c_str() );loadShortcutSettings
    
    

// -----------------------------------

void ofApp::loadShortcutSettings( ) {
    int numberOfShortcuts;
    shortcutData.pushTag( "SETTINGS", 0 );
    numberOfShortcuts = shortcutData.getNumTags( "shortcut" );
    ofLogNotice( "###################### " + ofToString( numberOfShortcuts ) + " shortcut nodes found" );
    for ( int i = 0; i < numberOfShortcuts; i++ ) {
//        ImGuiKey tt = (ImGuiKey) shortcutData.getValue( "shortcut:button", 0, i );
        string tempname = shortcutData.getValue( "shortcut:button", "", i );
        ofLogNotice( "1: " + tempname );
        string tempname2 = ofToString (shortcutData.getValue( "shortcut:key", 0, i ) );
        ofLogNotice( "2: " + tempname2 );
        
        shortcutList[ tempname ] = (ImGuiKey) shortcutData.getValue( "shortcut:key", 0, i );
    }
    shortcutData.popTag( );
}


// -----------------------------------

void ofApp::loadScreenSettings( ) {
    int numberOfScreens;
    screenData.pushTag( "SETTINGS", 0 );

    numberOfScreens = screenData.getNumTags( "screen" );
    ofLogNotice( ofToString( numberOfScreens ) + " screen nodes found" );
    for ( int i = 0; i < numberOfScreens; i++ ) {
        //the last argument of getValue can be used to specify
        //which tag out of multiple tags you are refering to.

        addScreen( ofPoint( screenData.getValue( "screen:x", 0, i ), screenData.getValue( "screen:y", 0, i ) ),
                screenData.getValue( "screen:width", 0, i ),
                screenData.getValue( "screen:height", 0, i ),
                screenData.getValue( "screen:pointsX", 0, i ),
                screenData.getValue( "screen:pointsY", 0, i ),
                screenData.getValue( "screen:pixelsPerGridDivision", 0, i )
                );

        screenManager.actualScreen = i + 1;
        screenManager.setScreenWidth( screenManager.actualScreen, screenData.getValue( "screen:width", 800, i ) );
        screenManager.setScreenHeight( screenManager.actualScreen, screenData.getValue( "screen:height", 600, i ) );
        screenManager.loadWarpSettings( false );
        ofImage tempMask;
        if ( screenData.getValue( "screen:mask", "", i ).size( ) ) {
            screenManager.setMask( i + 1, screenData.getValue( "screen:mask", "", i ) );
        } else {
            ofLogNotice( "No mask scheduled for screen number " + ofToString( i ) );
        }


    }
    screenManager.actualScreen = 0;
    screenData.popTag( );

}


// -----------------------------------

void ofApp::saveSlotSettings( ) {
    saveSlotSettings( 0 );
}
// -----------------------------------

void ofApp::saveSlotSettings( int sceneNumber ) {
    ofLogNotice( "prepare slot settings:" );
    slotData.clear( );
    slotData.addTag( "SETTINGS" );
    if ( slotData.pushTag( "SETTINGS" ) ) {
        for ( unsigned int i = 0; i < slots.size( ); i++ ) {
            ofLogNotice( "add node " + ofToString( i ) );

            int tagNum = slotData.addTag( "slot" );
            slotData.setValue( "slot:type", (int) slots[i]->getSlotType( ), tagNum );
            slotData.setValue( "slot:width", settings.getIntValue( "contentWidth" ), tagNum );
            slotData.setValue( "slot:height", settings.getIntValue( "contentHeight" ), tagNum );
            slotData.setValue( "slot:color:r", slotColors[i].x, tagNum );
            slotData.setValue( "slot:color:g", slotColors[i].y, tagNum );
            slotData.setValue( "slot:color:b", slotColors[i].z, tagNum );
            slotData.setValue( "slot:path", slots[i]->getSlotPath( ), tagNum );
            slotData.setValue( "slot:paused", videoPaused[i], tagNum );
            slotData.setValue( "slot:loop", slots[i]->getVideoLoopState( ), tagNum );
            slotData.setValue( "slot:speed", slots[i]->getVideoSpeed( ), tagNum );
            // target Paths will not be saved. later, when loading, everything is loaded to target values, then transition is called
        }
    }
    slotData.popTag( );
    string path = "";
    if ( sceneNumber > 0 ) {
        ofLogNotice( "trying to save slot settings - for scene " + ofToString( sceneNumber ) );
        path = "scenes/" + ofToString( sceneNumber );
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            dir.create( true );
        }
    } else {
        ofLogNotice( "trying to save main slot settings (for startup)" );
        path = "slots";
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            dir.create( true );
        }
    }
    path += "/slotData.xml";
    slotData.save( path );
    slotsWindow_unsaved = false; 
}

// -----------------------------------

void ofApp::saveSceneScreenSettings( int sceneNumber ) {
    ofLogNotice( "prepare screen settings (autoload) for scene " + ofToString( sceneNumber ) );
    sceneScreenData.clear( );
    sceneScreenData.addTag( "SETTINGS" );
    if ( sceneScreenData.pushTag( "SETTINGS" ) ) {
        for ( unsigned int i = 0; i < screenManager.getScreenCount( ); i++ ) {
            ofLogNotice( "add node " + ofToString( i ) );
            int tagNum = sceneScreenData.addTag( "screen" );
            sceneScreenData.setValue( "screen:type", screenManager.getSourceType( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:width", settings.getIntValue( "contentWidth" ), tagNum );
            sceneScreenData.setValue( "screen:height", settings.getIntValue( "contentHeight" ), tagNum );
            sceneScreenData.setValue( "screen:color:r", screenManager.getColor( i + 1 ).r, tagNum );
            sceneScreenData.setValue( "screen:color:g", screenManager.getColor( i + 1 ).g, tagNum );
            sceneScreenData.setValue( "screen:color:b", screenManager.getColor( i + 1 ).b, tagNum );
            sceneScreenData.setValue( "screen:path", screenManager.getPath( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:paused", screenManager.getVideoPaused( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:loop", screenManager.getVideoLoopState( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:speed", screenManager.getVideoSpeed( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:fadetime", screenManager.getFadeTime( i + 1 ), tagNum );
            sceneScreenData.setValue( "screen:masterAlpha", screenManager.getMasterAlpha( i + 1 ), tagNum );
        }
    }
    sceneScreenData.popTag( );
    string path = "";
    if ( sceneNumber > 0 ) {
        ofLogNotice( "trying to save screen settings (autoload) for scene " + ofToString( sceneNumber ) );
        path = "scenes/" + ofToString( sceneNumber );
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            dir.create( true );
        }
    } else {
        // this should never happen, should it?
        ofLogNotice( "trying to save main screen settings (for startup)" );
        path = "screens";
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            dir.create( true );
        }
    }
    path += "/screenAutoloadData.xml";
    sceneScreenData.save( path );
}



// -----------------------------------

void ofApp::loadSlotSettings( ) {
    loadSlotSettings( 0 );
}

// -----------------------------------

void ofApp::loadSlotSettings( int sceneNumber ) {
    string path = "";
    if ( sceneNumber > 0 ) {
        ofLogNotice( "trying to load slot settings - for scene " + ofToString( sceneNumber ) );
        path = "scenes/" + ofToString( sceneNumber );
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            ofLogError( "ERROR! no folder for slot settings - for scene " + ofToString( sceneNumber ) );
        }
    } else {
        ofLogNotice( "trying to load main slot settings (for startup)" );
        path = "slots";
    }
    path += "/slotData.xml";

    if ( slotData.loadFile( path ) ) {
        ofLogNotice( ofToString( path ) + " loaded!" );
    } else {
        ofLogError( "ERROR! Unable to Load " + ofToString( path ) + "!" );
    }

    int numberOfSlots;
    slotData.pushTag( "SETTINGS", 0 );
    numberOfSlots = slotData.getNumTags( "slot" );
    ofLogNotice( ofToString( numberOfSlots ) + " slot nodes found. Compare with reality..." );

    // ToDo: slots check? NO! There's no reality here, only XML
    //    int numberOfSlotsReality = slots.size();
    //    
    //    if (numberOfSlotsReality < numberOfSlots) {
    //        ofLogError(ofToString(numberOfSlotsReality) + " slots in Reality found. BAD! Ignoring surplus slots from XML" );
    //        numberOfSlots = numberOfSlotsReality;
    //    } else if (numberOfSlotsReality > numberOfSlots) {
    //        ofLogError(ofToString(numberOfSlotsReality) + " slots in Reality found. NOT GOOD! Ignoring surplus slots in reality" );
    //    } else {
    //        ofLogNotice(ofToString(numberOfSlots) + "... " + ofToString(numberOfSlots) + " real slots found. Looks Good. ");
    //    }

    for ( int i = 0; i < numberOfSlots; i++ ) {
        // ad a slot if called on startup (=not on loading a scene)
        // TODO: add slot addition and non-addition and so on depending on the number of slots already there and in the file
        if ( sceneNumber <= 0 ) {
            addSlot( );
        }
        // set stuff
        slots[i]->setSlotType( slotData.getValue( "slot:type", 0, i ) );

        ofLogNotice( "fetch r; " + ofToString( slotData.getValue( "slot:color:r", 0.0, i ) ) );
        slots[i]->color.r = slotData.getValue( "slot:color:r", 0.0, i );
        slots[i]->color.g = slotData.getValue( "slot:color:g", 0.0, i );
        slots[i]->color.b = slotData.getValue( "slot:color:b", 0.0, i );

        ofLogNotice( "slot set to color " + ofToString( slots[i]->color ) );
        videoPaused[i] = slotData.getValue( "slot:paused", 0, i );
        if ( slotData.getValue( "slot:loop", 0, i ) ) {
            slots[i]->setVideoLoopState( OF_LOOP_NORMAL );
        } else {
            slots[i]->setVideoLoopState( OF_LOOP_NONE );
        }
        slots[i]->setVideoSpeed( slotData.getValue( "slot:speed", 0, i ) );

        slots[i]->setSlotPath( slotData.getValue( "slot:path", "", i ) );
        
    }
    slotData.popTag( );
    //    for (unsigned int i = 0; i < screenManager.getScreenCount(); i++) {
    //        startTransitionForScreen(i + 1);
    //    }

}

// -----------------------------------

void ofApp::loadScreensForScene( int sceneNumber ) {
    string path = "";
    if ( sceneNumber > 0 ) {
        ofLogNotice( "trying to load screen settings - for scene " + ofToString( sceneNumber ) );
        path = "scenes/" + ofToString( sceneNumber );
        ofDirectory dir( path );
        if ( !dir.exists( ) ) {
            ofLogError( "ERROR! no folder for screen settings - for scene " + ofToString( sceneNumber ) );
        }
    } else {
        ofLogNotice( "trying to load main screen settings (for startup)" );
        path = "screens";
    }
    path += "/screenAutoloadData.xml";

    if ( sceneScreenData.loadFile( path ) ) {
        ofLogNotice( ofToString( path ) + " loaded!" );
    } else {
        ofLogError( "ERROR! Unable to Load " + ofToString( path ) + "!" );
    }

    int numberOfScreens;
    sceneScreenData.pushTag( "SETTINGS", 0 );
    numberOfScreens = sceneScreenData.getNumTags( "screen" );
    ofLogNotice( ofToString( numberOfScreens ) + " screen nodes found. Compare with reality..." );
    int numberOfScreensReality = screenManager.getScreenCount( );

    if ( numberOfScreensReality < numberOfScreens ) {
        ofLogError( ofToString( numberOfScreensReality ) + " screens in Reality found. BAD! Ignoring surplus screens from XML" );
        numberOfScreens = numberOfScreensReality;
    } else if ( numberOfScreensReality > numberOfScreens ) {
        ofLogError( ofToString( numberOfScreensReality ) + " screens in Reality found. NOT GOOD! Ignoring surplus screens in reality" );
    } else {
        ofLogNotice( ofToString( numberOfScreens ) + "... " + ofToString( numberOfScreens ) + " real screens found. Looks Good. " );
    }


    for ( int i = 0; i < numberOfScreens; i++ ) {
        // ad a screen if called on startup (=not on loading a scene)
        // TODO: add screen addition and non-addition and so on depending on the number of screens already there and in the file
        if ( sceneNumber <= 0 ) {
            addSlot( );
        }
        // set stuff

        ofLoopType loopType = (ofLoopType) sceneScreenData.getValue( "screen:loop", OF_LOOP_NORMAL, i );
        screenManager.setTargetSource(
                i + 1,
                sceneScreenData.getValue( "screen:type", 0, i ),
                sceneScreenData.getValue( "screen:path", "", i ),
                ofFloatColor(
                sceneScreenData.getValue( "screen:color:r", 0.3f, i ),
                sceneScreenData.getValue( "screen:color:g", 0.3f, i ),
                sceneScreenData.getValue( "screen:color:b", 0.3f, i )
                ),
                sceneScreenData.getValue( "screen:fadetime", 1, i ),
                loopType,
                sceneScreenData.getValue( "screen:speed", 1, i ),
                masterSpeed,
                sceneScreenData.getValue( "screen:masterAlpha", 255, i )
                // ToDo: add paused?
                );
    }
    sceneScreenData.popTag( );
}



// -----------------------------------

void ofApp::setButtonColor( int startHsv ) {
    setButtonColor( startHsv, (int) 0, 1.0f, false );
}
// -----------------------------------

void ofApp::setButtonColor( int startHsv, bool shortcutable ) {
    setButtonColor( startHsv, 0, 1.0f, shortcutable );
}
// -----------------------------------

void ofApp::setButtonColor( int startHsv, int adjustIterator ) {
    setButtonColor( startHsv, adjustIterator, 1.0f, false );
}
// -----------------------------------

void ofApp::setButtonColor( int startHsv, int adjustIterator, bool shortcutable ) {
    setButtonColor( startHsv, adjustIterator, 1.0f, shortcutable );
}

// -----------------------------------

void ofApp::setButtonColor( int startHsv, int adjustIterator, float multiplier ) {
    setButtonColor( startHsv, adjustIterator, multiplier, 0.6f, false );
}
// -----------------------------------

void ofApp::setButtonColor( int startHsv, int adjustIterator, float multiplier, bool shortcutable ) {
    setButtonColor( startHsv, adjustIterator, multiplier, 0.6f, shortcutable );
}

// -----------------------------------

void ofApp::setButtonColor( int startHsv, int adjustIterator, float multiplier, float saturation, bool shortcutable ) {
    if ( shortcutState == OE_SHORTCUT_NONE || !shortcutable ) {
        ImGui::PushStyleColor( ImGuiCol_Button, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, saturation, 0.4f ) );
    } else {
        ImGui::PushStyleColor( ImGuiCol_Button, (ImVec4) ImColor::HSV( 22 / 19.0f, 0.5 , 0.6f  ) );
    }
    ImGui::PushStyleColor( ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, saturation + 0.1f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, saturation + 0.2f, 0.6f ) );
}

// -----------------------------------

void ofApp::setSliderColor( int startHsv ) {
    setSliderColor( startHsv, 0, 1.0f );
}

// -----------------------------------

void ofApp::setSliderColor( int startHsv, int adjustIterator ) {
    setSliderColor( startHsv, adjustIterator, 1.0f );
}

// -----------------------------------

void ofApp::setSliderColor( int startHsv, int adjustIterator, float multiplier ) {
    ImGui::PushStyleColor( ImGuiCol_FrameBg, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.6f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_FrameBgHovered, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.65f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_FrameBgActive, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.7f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_SliderGrab, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.8f, 0.9f ) );
    ImGui::PushStyleColor( ImGuiCol_SliderGrabActive, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.95f, 0.9f ) );
}


// -----------------------------------

void ofApp::setHighlightButtonColor( int startHsv ) {
    setHighlightButtonColor( startHsv, 0, 1.0f, false );
}
// -----------------------------------

void ofApp::setHighlightButtonColor( int startHsv, bool shortcutable ) {
    setHighlightButtonColor( startHsv, 0, 1.0f, shortcutable );
}

// -----------------------------------

void ofApp::setHighlightButtonColor( int startHsv, int adjustIterator ) {
    setHighlightButtonColor( startHsv, adjustIterator, 1.0f, false );
}

// -----------------------------------

void ofApp::setHighlightButtonColor( int startHsv, int adjustIterator, bool shortcutable ) {
    setHighlightButtonColor( startHsv, adjustIterator, 1.0f, shortcutable );
}
// -----------------------------------

void ofApp::setHighlightButtonColor( int startHsv, int adjustIterator, float multiplier, bool shortcutable ) {
    if ( shortcutState == OE_SHORTCUT_NONE || !shortcutable ) {
        ImGui::PushStyleColor( ImGuiCol_Button, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.6f, 0.8f ) );
    } else {
        ImGui::PushStyleColor( ImGuiCol_Button, (ImVec4) ImColor::HSV( 2 / 19.0f, 0.5 , 0.6f  ) );
    }
    ImGui::PushStyleColor( ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.7f, 0.8f ) );
    ImGui::PushStyleColor( ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.8f, 0.7f ) );
}

// -----------------------------------

void ofApp::setInactiveButtonColor( int startHsv ) {
    setInactiveButtonColor( startHsv, 0, 1.0f );
}

// -----------------------------------

void ofApp::setInactiveButtonColor( int startHsv, int adjustIterator ) {
    setInactiveButtonColor( startHsv, adjustIterator, 1.0f );
}

// -----------------------------------

void ofApp::setInactiveButtonColor( int startHsv, int adjustIterator, float multiplier ) {
    ImGui::PushStyleColor( ImGuiCol_Button, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.1f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_ButtonHovered, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.2f, 0.5f ) );
    ImGui::PushStyleColor( ImGuiCol_ButtonActive, (ImVec4) ImColor::HSV( (multiplier * adjustIterator + startHsv) / 19.0f, 0.3f, 0.4f ) );
}

//--------------------------------------------------------------
/// direct message handling
/// note: this is called on the MIDI thread, so copy message contents

void ofApp::newMidiMessage( ofxMidiMessage &message ) {

    // add the latest message to the message queue
    midiMessages.push_back( message );

    // remove any old messages if we have too many
    while ( midiMessages.size( ) > maxMessages ) {
        midiMessages.erase( midiMessages.begin( ) );
    }
    if ( message.control == 12 ) {
        masterAlpha = ofMap( message.value, 0, 127, 0, 1.0 );
    }


}


//--------------------------------------------------------------

void ofApp::exit( ) {

    if ( useMidi ) {
        // clean up
        midiIn.closePort( );
        midiIn.removeListener( this );
        externalList.exit( );
    }
}
