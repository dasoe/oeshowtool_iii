#pragma once
#include "ofMain.h"

enum oeSlotType : const unsigned int {
    OE_COLOR = 0,
    OE_IMAGE = 1,
    OE_VIDEO = 2,
    OE_EXTERNAL = 3,
};


class oeSlot {
public:


    void init();
    void update();
    //void prepareContent();
    void selectSource();
    void setSlotType(oeSlotType transType);
    void setSlotType(int transType);
    void setSlotPath(string transPath);
    string getSlotPath();
    
    
    void setVideoPaused(bool paused);
    void setVideoPosition(float percent);
    float getVideoPosition();

    void restartVideo();
    void restartTargetVideo();
    void setVideoLoopState(ofLoopType state);
    ofLoopType getVideoLoopState();
    void setTargetVideoLoopState(ofLoopType state);
    void setVideoSpeed(float speed);
    float getVideoSpeed();
    void startTransitionForSlot();
    
    oeSlotType getSlotType();
    oeSlotType getTargetSlotType();
    bool getIsActive();
    
    string getInfo();
    string getMoviePath();

    bool rewindOnTransition, mainVideo;
    ofLoopType videoLoopstate;

    string imagePath, targetImagePath, videoPath, targetVideoPath;
    
    ofFloatColor color;
    ofFloatColor targetColor;
    
    int targetAlpha;

    float contrast, brightness, saturation;
    float videoSpeed;

    long actualTime, fadeTimerForSlot, slotIsActiveTimer;
    int fadeTimeForSlot;

    oeSlotType type;
    oeSlotType targetType;
    string path;    

    
//    void clearAutoLoadScenes();
//    void addAutoLoadScene();
//    vector <int> getAutoLoadScenes();
//    
//    vector <int> autoLoadScenes;    

};
