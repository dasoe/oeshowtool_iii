#pragma once

#include "ofMain.h"
#include "ofxBezierWarp.h"
#include "ofxXmlSettings.h"
#include "oeScreenManager.h"
#include "oeBezierEditor.h"
#include "ofxImGui.h"
#include "ofxXmlBasedProjectSettings.h"
#include "ofxFlashMessages.h"
#include "oeSlot.h"
#include "oeScene.h"
//#include "ofxTimeMeasurements.h"
#include "oeExternalList.h"
#include "ofxMidi.h"


enum oeShowToolHsvOptions: const unsigned int {
    OE_HSV_GREEN = 6,
    OE_HSV_PETROL = 9,
    OE_HSV_BLUE = 11,
    OE_HSV_MAUVE = 16,
    OE_HSV_RED = 18,
    OE_HSV_ORANGE = 20
};

enum oeShowToolShortcutState: const unsigned int {
    OE_SHORTCUT_NONE = 0,
    OE_SHORTCUT_SETKEY = 1,
    OE_SHORTCUT_SETBUTTON = 2,
};

class ofApp : public ofBaseApp, public ofxMidiListener {
public:
    void setup();    
    void update();
    void exit();

    void cycledUpdate();
    void draw();
    void drawScreensGuiPart();
    void drawSlotGuiPart();
    void drawScenesGuiPart();
    void drawMasterGuiPart();
    void drawKeystrokeGuiPart();
    void drawDemoWindowGuiPart();
    void adjustMasterSpeed();
    void listenToKeyShortCut();
    void keyPressed(int key);
    void keyReleased( int key );
    bool makeItAShortcut( string id, string name ); 

    void addScreen(ofPoint position, int width, int height, int pointsX, int pointsY, int pixelsPerGridDivision);    
    void removeScreen(int tempScreenNumber);    
    void loadScreenSettings();    
    void saveScreenSettings();
    
    void loadShortcutSettings();
    void saveShortcutSettings();
    
    void saveSlotSettings();

    void saveSlotSettings(int sceneNumber);
    void saveSceneScreenSettings(int sceneNumber);    
    void loadSlotSettings();
    void loadSlotSettings(int sceneNumber);

    void addSlot();

    void setButtonColor(int startHsv);
    void setButtonColor(int startHsv, bool shortcutable);
    void setButtonColor(int startHsv, int adjustIterator);
    void setButtonColor(int startHsv, int adjustIterator, bool shortcutable);
    void setButtonColor(int startHsv, int adjustIterator, float multiplier);
    void setButtonColor(int startHsv, int adjustIterator, float multiplier, bool shortcutable);
    void setButtonColor(int startHsv, int adjustIterator, float multiplier, float saturation, bool shortcutable);
    
    void setHighlightButtonColor(int startHsv);
    void setHighlightButtonColor(int startHsv, bool shortcutable);
    void setHighlightButtonColor(int startHsv, int adjustIterator);
    void setHighlightButtonColor(int startHsv, int adjustIterator, bool shortcutable);
    void setHighlightButtonColor(int startHsv, int adjustIterator, float multiplier, bool shortcutable);
        
    void setSliderColor(int startHsv);
    void setSliderColor(int startHsv, int adjustIterator);
    void setSliderColor(int startHsv, int adjustIterator, float multiplier);
    
    void setInactiveButtonColor(int startHsv);
    void setInactiveButtonColor(int startHsv, int adjustIterator);
    void setInactiveButtonColor(int startHsv, int adjustIterator, float multiplier);
    
    void addScene();   
    void loadSlotsForScene(int sceneNumber);   
    void loadScreensForScene(int sceneNumber);
    void startScene(int sceneNumber);
    void saveSceneSettings(int sceneNumber);
    void prepareInnerSceneSettingsForSave(int tagNumber, int sceneNumber);
    void loadSceneSettings();

    void addFeedback(string _message, ofLogLevel _level);
    void addFeedback(string _message, ofLogLevel _level, bool _showFlashMessage);

    
    void listDirectories();
    void listMaskDirectory();
    void listBezierDirectory();

    int windowInited;
    
    long actualTime, cycleTimer;
    int cycledUpdateTimeDelta;
    
    oeShowToolShortcutState shortcutState;
    map< string, ImGuiKey> shortcutList; 
    ImGuiInputFlags shortcutFlags;
    
    ofDirectory bezierDir, maskDir, videoDir, imageDir;
    ofxImGui::Gui gui;

    ofOeCorner chosenCorner;
    int chosenCornerInt, lastChosenCornerInt;

    int sceneCounter, firstMonitorWidth, firstMonitorHeight;
    int actualScreen;
    float masterSpeed, lastMasterSpeed, masterSpeedMultiplier, lastMasterSpeedMultiplier, masterAlpha, imGuiSpacing;
    ImVec4 startSlotColor;
    
    ofxXmlBasedProjectSettings settings;
    ofxXmlSettings slotData, sceneScreenData, screenData, sceneData, shortcutData;
    
    bool useFeedbackWindow, drawGui;
    vector <int> tempPressedKeys;
   
    

    oeScreenManager screenManager;
    int screenCreatePositionAndSize[4], screenCreateGrid[3], removeScreenNumber;
    bool show_test_window, actualScreenSoloMode, showGridOnControlScreen;
    bool shortcutWindow_unsaved, screensWindow_unsaved, sceneseWindow_unsaved, slotsWindow_unsaved;

    vector <oeSlot*> slots;
    vector <oeScene*> scenes;
    int actualScene;
    vector <string> tempSceneName;
    vector <int> tempSceneNumber;
    string sceneMessage;

    vector <int> slotNumberForScreen;
    vector <int> slotTypes;
    vector <string> targetSlotPaths;
    vector <ImVec4> slotColors;
    vector <ImVec4> targetSlotColors;

    oeExternalList externalList;
    vector<std::string> tExternalList;

    vector <bool> videoPaused;
    vector <bool> videoLooping;
    vector <float> videoSpeed;
    vector <bool> targetVideoPaused;
    vector <bool> targetVideoLooping;
    vector <float> targetVideoSpeed;
    
    vector <ofImage> masks;
    vector <string> maskPaths;
    int maskBlur;
    
    
    // fbos holding the actual image
    oeBezierEditor bezier;

    ofxFlashMessages flashMessages;
    ofFbo feedbackFBO;
    GLuint textureSourceID;

    ImGuiKey tempkey;
    // #### MIDI #####
    
    /// direct message handling
    /// ofxMidiListener callback, called by ofxMidiIn instance if set as listener
    /// note: this is not needed if you use queued message passing
    void newMidiMessage(ofxMidiMessage &message);

    bool useMidi;
    int midiPort;
    ofxMidiIn midiIn;
    std::vector<ofxMidiMessage> midiMessages; ///< received messages
    std::size_t maxMessages = 10; ///< max number of messages to keep track of

    string spoutPrefix;
    string syphonPrefix;
    string ndiPrefix;
    

    
};
