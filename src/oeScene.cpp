#include "oeScene.h"


//--------------------------------------------------------------

void oeScene::addScene(vector <int> tempTargetScreenToSlotValues) {
    ofLogNotice("add scene with " + ofToString(tempTargetScreenToSlotValues.size()) + " slot to screen elements");
    setSlots(tempTargetScreenToSlotValues);
    name = "new";
}

string oeScene::getName() {
    return name;
}

//--------------------------------------------------------------

int oeScene::getNumber() {
    return number;
}

//--------------------------------------------------------------

void oeScene::setName(string tempName) {
    ofLogNotice("set name for scene: " + tempName);
    name = tempName;
}

//--------------------------------------------------------------

void oeScene::setNumber(int tempNumber) {
    ofLogNotice("set number for scene: " + ofToString(tempNumber));
    number = tempNumber;
}

//--------------------------------------------------------------

void oeScene::update() {

}


//--------------------------------------------------------------

void oeScene::setSlots(vector <int> tempTargetScreenToSlotValues) {
    ofLogNotice("set " + ofToString(tempTargetScreenToSlotValues.size()) + " slots for scene: ");

    slotNumberForScreen = tempTargetScreenToSlotValues;
}

//--------------------------------------------------------------

vector <int> oeScene::getSlots() {
    return slotNumberForScreen;
}
