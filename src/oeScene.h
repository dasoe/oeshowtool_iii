#pragma once
#include "ofMain.h"

class oeScene {
public:

    void addScene(vector <int> tempSlotNumberForScreen);
    void setSlots(vector <int> tempSlotNumberForScreen);
    vector <int> getSlots();

    void update();
    string getName();
    void setName(string tempName);
    int getNumber();
    void setNumber(int tempNumber);

    string name;
    int number;
    float fadeToTime;
    vector <int> slotNumberForScreen;

};