#pragma once
#include "ofMain.h"

#ifdef TARGET_WINVS
#include "ofxSpout.h"
#endif

#ifdef USE_NDI
#include "ofxNDI.h" // NDI classes
#endif


class oeExternalSource
{
public:
    void setup(string path, int width, int height );
    void update();
    void draw(int x, int y, int contentWidth, int contentHeight);
    bool isInitialized();
    void close();
    void exit();

#ifdef TARGET_WINVS
    ofxSpout::Receiver spoutReceiver;
#endif
    
#ifdef USE_NDI
    ofxNDIreceiver ndiReceiver; // NDI receiver    
#endif
    ofTexture texture;

};

