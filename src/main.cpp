#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main() {

    ofGLFWWindowSettings settings;
    //settings.windowMode = OF_FULLSCREEN; //can also be OF_FULLSCREEN
    settings.multiMonitorFullScreen = true;
    settings.decorated = false;


    // Linux version only: Icon:
    //ofApp *app = new ofApp();

    //ofAppGLFWWindow* win;
    //win = dynamic_cast<ofAppGLFWWindow *> (ofGetWindowPtr());
    //win->setWindowIcon("icon/oe_64-64_icon.png");

    //ofRunApp(app);

    auto window = ofCreateWindow(settings);

    ofRunApp(new ofApp());

}

