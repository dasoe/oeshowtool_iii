#include "oeUtilities.h"

//--------------------------------------------------------------

string oeUtilities::typeToString(int type) {
    string out;
    switch (type) {
        case 1:
            out = "Image";
            break;
        case 2:
            out = "Video";
            break;
        case 3:
            out = "External";
            break;
        default:
            out = "Color";
            break;
    }
    return out;
    
}