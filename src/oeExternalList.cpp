#include "oeExternalList.h"	

//--------------------------------------------------------------
void oeExternalList::init() {
#ifdef USE_NDI
    
    ofLogNotice( ndiReceiver.GetNDIversion() );
    ndiReceiver.FindSenders();    
#endif
    
}

//--------------------------------------------------------------
vector<std::string> oeExternalList::list() {

#ifdef TARGET_WINVS
    spoutReceiver.init();
    //sourcesLists['spout'] = spoutReceiver.getAvailableSenders();
    return spoutReceiver.getAvailableSenders();

#endif

#ifdef USE_NDI

    ofLogNotice( ndiReceiver.GetNDIversion() );
    ndiReceiver.FindSenders();    

    vector<std::string> tList;

    int nsenders = ndiReceiver.GetSenderCount();

    //ofLogNotice("Amount of senders: " + ofToString(nsenders) );

        	char name[256];

		if (nsenders > 0) {
			std::cout << "Number of NDI senders found: " << nsenders << std::endl;
			for (int i = 0; i < nsenders; i++) {
				ndiReceiver.GetSenderName(name, 256, i);
				std::cout << "    Sender " << i << " [" << name << "]" << std::endl;
			}
                        tList = ndiReceiver.GetSenderList();
		}
		else
			std::cout << "No NDI senders found" << std::endl;
        
    
    
    sourcesLists['ndi'] = tList;
    return tList;

#endif
   vector <std::string> torf;
return torf ;

}

//--------------------------------------------------------------
void oeExternalList::exit() {

#ifdef USE_NDI
    ofLogNotice("##releasing list receiver");
    ndiReceiver.ReleaseReceiver();
#endif

}