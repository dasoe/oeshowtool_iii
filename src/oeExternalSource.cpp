#include "oeExternalSource.h" 


//--------------------------------------------------------------

void oeExternalSource::setup( string path, int width, int height ) {


#ifdef TARGET_WINVS
    spoutReceiver.init( path );
#endif
#ifdef USE_NDI

    //optionally you can specify a channel name e.g.
    //spoutReceiver.init("Camera");
    ofLogNotice( ndiReceiver.GetNDIversion() );
    
    ofLogNotice( ofToString( ndiReceiver.FindSenders() ) );    
    
//    
//    std::cout << "Selected [" << ndiReceiver.GetSenderName( (char*) path.c_str() ) << "]" << " | " << path << std::endl;
//    ndiReceiver.SetSenderIndex( 0 );
//    std::cout << "Selected [" << ndiReceiver.GetSenderName( (char*) path.c_str() ) << "]" << " | " << path << std::endl;
    std::cout << "xxxx############xxxxxxxx Senders: " << ndiReceiver.GetSenderCount();
    
        ndiReceiver.SetSenderIndex(0);
        texture.allocate(width, height, GL_RGBA);
#endif

    
}

//--------------------------------------------------------------

void oeExternalSource::update( ) {
#ifdef TARGET_WINVS
	spoutReceiver.receive(texture);
#endif
#ifdef USE_NDI
    ndiReceiver.ReceiveImage( texture );
#endif
}

// -----------------------------------

void oeExternalSource::draw(int x, int y, int contentWidth, int contentHeight) {
    texture.draw( 0, 0, contentWidth, contentHeight );
}


// -----------------------------------

void oeExternalSource::close( ) {
#ifdef TARGET_WINVS
    spoutReceiver.release( );
#endif
#ifdef USE_NDI
    ndiReceiver.ReleaseReceiver();
#endif
}

//--------------------------------------------------------------
void oeExternalSource::exit() {
#ifdef USE_NDI
    ndiReceiver.ReleaseReceiver();
#endif
}

// -----------------------------------

bool oeExternalSource::isInitialized( ) {
#ifdef TARGET_WINVS
    return spoutReceiver.isInitialized( );
#endif
#ifdef USE_NDI    
    return ndiReceiver.ReceiverConnected();
#endif
    
return false;
}
