#pragma once
#include "ofMain.h"



enum oeParameterType: const unsigned int {
	OE_INT = 1,
	OE_FLOAT = 2
};

class oeParameter {
public:

    void add(const oeParameter& orig);

    string name;
    float value,min,max;
    oeParameterType type;
    
};