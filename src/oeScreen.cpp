#include "oeScreen.h"
#include "ofGstVideoPlayer.h"

oeScreen::oeScreen() {
}

//--------------------------------------------------------------

void oeScreen::init(int width, int height) {
    contentFbo.allocate(width, height, GL_RGBA);
    fbo.allocate(width, height, GL_RGBA);
    brightness = 1.0f;
    contrast = 1.0f;
    saturation = 1.0f;	
    // load shader (brightness aso adjusting)
    
    if (brcosaShader.load("shaders/brcosa_GLSL")) {
        ofLogNotice("loaded brcosa Shader");
    } else {
        ofLogError("fails to load brcosa Shader. Please check: shaders/brcosa_GLSL");
    }
    
    // load placeholder video and image (prevent empty)
    mainImage = 0;
    mainVideo = 0;
    mainExternal = 0;
    masterAlpha = 255;

    tintColor = ofVec3f(0,0,0);

    video[mainVideo].setPlayer(ofPtr<ofGstVideoPlayer>(new ofGstVideoPlayer));
    video[!mainVideo].setPlayer(ofPtr<ofGstVideoPlayer>(new ofGstVideoPlayer));

    //    string placeholder = "required/placeholder.jpg"; 
//    image[mainImage].load(placeholder);
//    image[!mainImage].load(placeholder);
//    placeholder = "required/placeholder.mp4"; 
//    video[mainVideo].loadAsync(placeholder);
//    video[!mainVideo].loadAsync(placeholder);


}


//--------------------------------------------------------------

void oeScreen::setWidth(int transWidth) {
    contentFbo.allocate(transWidth, this->getHeight(), GL_RGBA);
    fbo.allocate(transWidth, this->getHeight(), GL_RGBA);
}

//--------------------------------------------------------------

void oeScreen::setHeight(int transHeight) {
    contentFbo.allocate(this->getWidth(), transHeight, GL_RGBA);
    fbo.allocate(this->getWidth(), transHeight, GL_RGBA);
}


//--------------------------------------------------------------

void oeScreen::update() {

    if (video[mainVideo].isLoaded()) {
        video[mainVideo].update();
    }

    if (video[!mainVideo].isLoaded()) {
        video[!mainVideo].update();
        // Hack, as speed is not set whilst loading
        if (video[!mainVideo].getSpeed() != baseSpeed) {
            video[!mainVideo].setSpeed(baseSpeed);
        }
    }

    if (external[mainExternal].isInitialized()) {
        external[mainExternal].update();
    }
    if (external[!mainExternal].isInitialized()) {
        external[!mainExternal].update();
    }

    actualTime = ofGetElapsedTimeMillis();

    if (targetMasterAlpha != masterAlpha) {

        if (masterAlpha < targetMasterAlpha) {
            masterAlpha += oneStepMasterAlpha;
        }
        else  if (masterAlpha > targetMasterAlpha) {
            masterAlpha -= oneStepMasterAlpha;
        }

        if (abs(targetMasterAlpha - masterAlpha) <= oneStepMasterAlpha) {
            masterAlpha = targetMasterAlpha;
        }
    }

    if (targetAlpha < 255) {
        
        float increment = (float) (actualTime - fadeTimerForSlot) / (float) (fadeTimeForSlot);
        //  ofLogNotice("fade increment: " + ofToString(ofLerp(0, 255, increment)));
        if (increment > 1.0) {
            increment = 1.0;
        }
        targetAlpha = ofLerp(0, 255, increment);
        
        //ofLogNotice("alpha: " + ofToString(targetAlpha));        

        if (targetAlpha > 254) {
            ofLogNotice("Transition of slot  DONE.");
            targetAlpha = 255;
            sourceType = targetSourceType;
            switch (targetSourceType) {
                case 1:
                    //Image
                    mainImage = !mainImage;
                    ofLogNotice( "swapped images, clearing target image");
                    image[!mainImage].clear();
                    break;
                case 2:
                    //Video
                    mainVideo = !mainVideo;
                    ofLogNotice( "swapped videos, clearing target video");
                    video[!mainVideo].close();
                    break;
                case 3:
                    //External
                    mainExternal = !mainExternal;
                    ofLogNotice("swapped Externals, clearing target external...  index:" + ofToString(!mainExternal));
                    external[!mainExternal].close();
                    break;
                default:
                    //Color
                    mainColor = !mainColor;
                    break;
            }
            path=targetPath;
        }
    }

    prepareContent();
    
}


//--------------------------------------------------------------

int oeScreen::getSourceType() {
    return sourceType;
}

//--------------------------------------------------------------

string oeScreen::getSourceInfo() {
    return oeUtil.typeToString(sourceType) + " | " + path;
}

//--------------------------------------------------------------

string oeScreen::getTargetSourceInfo() {
    return oeUtil.typeToString(targetSourceType) + " | " + targetPath;
}

//--------------------------------------------------------------

int oeScreen::getTargetSourceType() {
    return targetSourceType;
}


//--------------------------------------------------------------

ofFloatColor oeScreen::getColor() {
    return color[mainColor];
}

string oeScreen::getPath() {
    return path;
}

int oeScreen::getFadeTime() {
    return fadeTimeForSlot;
    return fadeTimeForSlot;
}


//--------------------------------------------------------------

void oeScreen::setTargetSource(int transType, string transPath, ofFloatColor transColor, int transFadeTime, ofLoopType transLoop, float transSpeed, float masterSpeed, float tmasterAlpha) {
    targetSourceType = transType;
    targetPath = transPath;
    fadeTimeForSlot = transFadeTime;
    baseSpeed = transSpeed;
    targetMasterAlpha = tmasterAlpha;
    if (!path.length()) {
        path=targetPath;
    }

    ofLogNotice("set targt source " + oeUtil.typeToString(targetSourceType) + " | " +targetPath + " | speed: " + ofToString(baseSpeed) );

    switch (targetSourceType) {
        case 1:
            //Image
            ofLogNotice("try to load image " + ofToString(targetPath));
            image[!mainImage].load(targetPath);
            ofLogNotice("loaded!");
            break;
        case 2:
            //Video
            ofLogNotice("try to load video " + ofToString(targetPath));
            video[!mainVideo].loadAsync(targetPath);
            video[!mainVideo].play();
            video[!mainVideo].setSpeed(baseSpeed*masterSpeed);
            video[!mainVideo].setVolume(0);
            setTargetVideoLoopState(transLoop);
            ofLogNotice("loaded!");

            break;
        case 3:
            ofLogNotice("init external... index:" + ofToString(!mainExternal) );
            external[!mainExternal].setup(targetPath, this->getWidth(),this->getHeight() );
            break;
        default:
            color[!mainColor] = transColor;
            ofLogNotice("color changed to " + ofToString(transColor));
            //Color
            break;
    }
    startTransition();    

}


//--------------------------------------------------------------

void oeScreen::startTransition() {
    // start transition - in case something has changed
    ofLogNotice("transition of Screen started");
    fadeTimerForSlot = ofGetElapsedTimeMillis();
    targetAlpha = 0;

    float absoluteVal = abs(masterAlpha - targetMasterAlpha);

    ofLogNotice("fadeTimeForSlot is " + ofToString(fadeTimeForSlot) + "  ofGetFrameRate() is " + ofToString(ofGetFrameRate()));

    oneStepMasterAlpha = absoluteVal / (float) (fadeTimeForSlot * ofGetFrameRate() / 1000) ;
    ofLogNotice("masterAlpha is " + ofToString(masterAlpha) + "  targetMasterAlpha is " + ofToString(targetMasterAlpha) + " ABS-Value: "  + ofToString(absoluteVal) + "  oneStepMasterAlpha is " + ofToString(oneStepMasterAlpha));
    // ToDo:
//    if (type == 2 && rewindOnTransition) {
//            restartTargetVideo();
//    }
}


//--------------------------------------------------------------

void oeScreen::setTargetVideoLoopState(ofLoopType state) {
    video[!mainVideo].setLoopState(state);
}

//--------------------------------------------------------------

void oeScreen::setVideoLoopState(ofLoopType state) {
    video[mainVideo].setLoopState(state);
}

//--------------------------------------------------------------

ofLoopType oeScreen::getVideoLoopState() {
    return video[mainVideo].getLoopState();
}

//--------------------------------------------------------------

bool oeScreen::getVideoPaused() {
    return video[mainVideo].isPaused();
}

//--------------------------------------------------------------

float oeScreen::getVideoSpeed() {
    return baseSpeed; //video[mainVideo].getSpeed();
}  

//--------------------------------------------------------------

void oeScreen::setVideoSpeed(float speed, float masterSpeed) {
    baseSpeed = speed;
    ofLogNotice("setVideoSpeed to " + ofToString(speed*masterSpeed));
    video[mainVideo].setSpeed(baseSpeed*masterSpeed);
}

//--------------------------------------------------------------

float oeScreen::getVideoPosition() {
    return video[mainVideo].getPosition();
}  

//--------------------------------------------------------------

void oeScreen::setVideoPosition(float position) {
    return video[mainVideo].setPosition(position);
}


//--------------------------------------------------------------

void oeScreen::setMasterAlpha(int talpha) {
    masterAlpha = talpha;
    targetMasterAlpha = masterAlpha;
}

//--------------------------------------------------------------

int oeScreen::getMasterAlpha() {
    return (int) masterAlpha;
}

//--------------------------------------------------------------

void oeScreen::oedraw(float x, float y, float w, float h) {

    if (masterAlpha) {

        bool doShader = false;
        this->begin();
            ofClear(0, 0, 0, 0);
    	    if (brightness != 1 ||
                contrast != 1 ||
                saturation != 1 || 
                (tintColor.x != 255 && tintColor.y != 255 && tintColor.z != 255) ||
                masterAlpha < 255
            ) {
                doShader = true;
            }
            if (doShader) {
    		    brcosaShader.begin();
		        brcosaShader.setUniform3f("avgluma", 0.62, 0.62, 0.62);                
    		    brcosaShader.setUniform1f("contrast", contrast);
    		    brcosaShader.setUniform1f("brightness", brightness);
    		    brcosaShader.setUniform1f("saturation", saturation);
		        brcosaShader.setUniform1f("alpha", ofMap((float)masterAlpha,0,255,0,1.0));
                brcosaShader.setUniform3f("tintColor", tintColor);
                if (contentFbo.isAllocated()) {
                    brcosaShader.setUniformTexture("imageMask", contentFbo.getTexture(), 1);
                }

  	        }
            //ofSetColor(255,255,255,masterAlpha);

            contentFbo.draw(0,0,w,h);
    	    if (doShader) {
    		    brcosaShader.end();
            }

        
         this->end();

    
        this->draw(x,y,w,h);
    }

}



//--------------------------------------------------------------

void oeScreen::setMask(string transPath) {
    maskPath = transPath;
    if (mask.load(transPath)) {
        mask.resize(this->getWidth(),this->getHeight());
        mask.setImageType(OF_IMAGE_COLOR_ALPHA);
        mask.getTexture().setSwizzle(GL_TEXTURE_SWIZZLE_A, GL_RED);         
        contentFbo.getTexture().setAlphaMask(mask.getTexture());
        
    } else {
        ofLogWarning("load Mask for actual screen failed"  );
        contentFbo.getTexture().disableAlphaMask();
    }        
}


//--------------------------------------------------------------

void oeScreen::setMaskPreview(ofImage transMaskPreview) {
    ofLogNotice("setMaskPreview "  );
    maskPreview = transMaskPreview;
    maskPreview.resize(this->getWidth(),this->getHeight());
    maskPreview.setImageType(OF_IMAGE_COLOR_ALPHA);
    maskPreview.getTexture().setSwizzle(GL_TEXTURE_SWIZZLE_A, GL_RED);         
    contentFbo.getTexture().setAlphaMask(maskPreview.getTexture());    
}


//--------------------------------------------------------------

void oeScreen::removeMaskPreview() {
    if (maskPath != "") {
        setMask(maskPath);
    } else {
        contentFbo.getTexture().disableAlphaMask();
    }
    
}


//--------------------------------------------------------------

ofImage oeScreen::getMask() {
    return mask;
}


//--------------------------------------------------------------

string oeScreen::getMaskPath() {
    return maskPath;
}



//string oeScreen::getInfo() {
//    string info = "";
//    if (getTargetSourceType() == 0) {
//        info = "COLOR ";
//    } else if (getTargetSourceType() == 1) {
//        string cleanedPath = targetImagePath;
//        ofStringReplace(cleanedPath, "images/", "");
//        info = "IMAGE " + cleanedPath;
//    } else if (getTargetSourceType() == 2 && video[!mainVideo].isLoaded()) {
//        string cleanedPath = video[!mainVideo].getMoviePath();
//        ofStringReplace(cleanedPath, "videos/", "");
//        info = "VIDEO " + cleanedPath;
//    }
//    return info;
//}




//--------------------------------------------------------------

void oeScreen::prepareContent() {
    
    int contentWidth = this->getWidth();
    int contentHeight = this->getHeight();

    contentFbo.begin();

    ofClear(0);

    // blend good in FBO Hack. Cross, not over dark
    // Thanks everybody here: https://forum.openframeworks.cc/t/fbo-problems-with-alpha/1643/12
	glPushAttrib(GL_ALL_ATTRIB_BITS);  
  
	glEnable(GL_BLEND);  
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,GL_ONE,GL_ONE_MINUS_SRC_ALPHA);  
    
    if (sourceType == 0) {

        ofSetColor(color[mainColor]);
        ofDrawRectangle(0, 0, contentWidth, contentHeight);
        ofSetColor(255, 255, 255, 255);

    } else if (sourceType == 1) {
       ofSetColor(255, 255, 255, 255);
        if (image[mainImage].isAllocated() ) {
        image[mainImage].draw(0, 0, contentWidth, contentHeight);
       }
    } else if (sourceType == 2) {
        ofSetColor(255, 255, 255, 255);
        if (video[mainVideo].isInitialized() ) {
            video[mainVideo].draw(0, 0, contentWidth, contentHeight);
        }
    }
    else if (sourceType == 3) {
        ofSetColor(255, 255, 255, 255);
        external[mainExternal].draw(0, 0, contentWidth, contentHeight);
    }

    // draw the target source (with actual Alpha) on top
    if (targetAlpha < 255) {
        if (targetSourceType == 0) {
            ofSetColor(color[!mainColor], targetAlpha);
            ofDrawRectangle(0, 0, contentWidth, contentHeight);
        } else if (targetSourceType == 1) {
            ofSetColor(255, 255, 255, targetAlpha);
            image[!mainImage].draw(0, 0, contentWidth, contentHeight);

        } else if (targetSourceType == 2) {

            ofSetColor(255, 255, 255, targetAlpha);

            if (video[!mainVideo].getCurrentFrame() > 2) {
                video[!mainVideo].draw(0, 0, contentWidth, contentHeight);
            }
        }
        else if (targetSourceType == 3) {
            ofSetColor(255, 255, 255, targetAlpha);
            external[!mainExternal].draw(0, 0, contentWidth, contentHeight);
        }
        ofSetColor(255, 255, 255, 255);
    }
        
        // blend good in FBO Hack Part II (End it)
	glDisable(GL_BLEND);  
	glPopAttrib();          

    contentFbo.end();

}

