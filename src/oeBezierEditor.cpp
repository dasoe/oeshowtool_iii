#include "oeBezierEditor.h"	


//--------------------------------------------------------------

void oeBezierEditor::init(int contentWidthTemp, int contentHeightTemp, int maskWidthTemp, int maskHeightTemp) {
    contentWidth = contentWidthTemp;
    contentHeight = contentHeightTemp;
    maskWidth = maskWidthTemp;
    maskHeight = maskHeightTemp;

    myBezier.setStrokeWeight(0);
    myBezier.setColorFill((ofColor(0)));
    
    ofLogNotice("maskFbo width:" + ofToString(maskWidth) + ", height: " + ofToString(maskHeight));

    maskFBO.allocate(maskWidth, maskHeight, GL_RGB);
    myBezier.setReactToMouseAndKeyEvents(false);
    isActive = false;
}

//--------------------------------------------------------------

void oeBezierEditor::setActive(bool active) {
    myBezier.setReactToMouseAndKeyEvents(active);
    isActive = active;
    ofEnableAlphaBlending();
}

//--------------------------------------------------------------

bool oeBezierEditor::getActive() {
    return isActive;
}


//--------------------------------------------------------------

void oeBezierEditor::loadBezier(string maskName) {
    myBezier.loadXmlPoints(maskName);
    ofLogNotice("bezier mask " + maskName + " loaded");
}

//--------------------------------------------------------------

void oeBezierEditor::saveMask(int blur) {
    ofLogNotice("mask: image saved");
    ofImage image;
    image = grabMask(blur);
    //
    ofFileDialogResult saveFileResult = ofSystemSaveDialog("bin/data/masks/example.jpg", "Maske sichern (Bsp.: 'XXX.jpg')");
    if (saveFileResult.bSuccess) {
        image.save(saveFileResult.filePath);
        ofLogNotice("mask: image saved");
    }
}

void oeBezierEditor::saveBezier(string path) {
    myBezier.saveXmlPoints(path);
}

//--------------------------------------------------------------

ofImage oeBezierEditor::grabMask(int blur) {
    //ofLogNotice("grabMask()");
    ofImage image;
    //image.allocate(contentWidth, contentHeight, OF_IMAGE_COLOR_ALPHA);
    ofPixels pixels;
    //ofLogNotice("grabMask() read To Pixels");
    maskFBO.readToPixels(pixels);
    image.setFromPixels(pixels);
    // cvImage.allocate(contentWidth, contentHeight);
    // ofLogNotice("grabMask() set CV IMage from pixels Pixels");

    cvImage.setFromPixels(pixels);
    cvImage.blur(blur);
    cvImage.invert();
    cvImage.resize(contentWidth, contentHeight);
    cvImage.flagImageChanged();
    ofLogNotice("cvimage size: " + ofToString(cvImage.getWidth()));
    image.setFromPixels(cvImage.getPixels());
    ofLogNotice("image size: " + ofToString(image.getWidth()));



    //screenGrab.grabScreen(0, 0, 1024, 768);
    return image;
}

//--------------------------------------------------------------
bool oeBezierEditor::getEditBezier() {
    return myBezier.getEditBezier();
}

//--------------------------------------------------------------

void oeBezierEditor::update() {
}

//--------------------------------------------------------------

void oeBezierEditor::draw() {
    if (isActive) {
        ofDisableAlphaBlending();
        maskFBO.begin();
        ofClear(0);
        ofDrawRectangle(0,0,maskWidth,maskHeight);
        myBezier.draw();
        maskFBO.end();
        ofSetColor(255);

        maskFBO.draw(0, 0);
        ofEnableAlphaBlending();
    }
}
//--------------------------------------------------------------

void oeBezierEditor::drawHelp() {

    myBezier.drawHelp();
    ofEnableAlphaBlending();
}
