/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "oeScreenManager.h"
#include "ofxOpenCv.h"

// -----------------------------------

void oeScreenManager::setup(int firstMonitorWidthTemp, int firstMonitorHeightTemp, int contentWidthTemp, int contentHeightTemp) {
    ofLogVerbose("oeScreenManager setup");

    actualScreen = 0;
    screenNumberCounter = 0;

    firstMonitorWidth = firstMonitorWidthTemp;
    firstMonitorHeight = firstMonitorHeightTemp;
    contentWidth = contentWidthTemp;
    contentHeight = contentHeightTemp;

    //  TODO: mask as shader?
    //  maskShader.load("shaders/dummyVert.c", "shaders/maskShaderFrag.c");

}

// -----------------------------------

void oeScreenManager::showGridOnControlScreenChanged(bool & active) {
    ofLogNotice("------ showGridOnControlScreenChanged -------");
    showGridOnControlScreen = active;
    setWarpGridPosition();
}

// -----------------------------------

void oeScreenManager::info() {
    ofLogNotice("------ oeScreenManager: -------");
    ofLogNotice("number of screens: " + ofToString(screens.size()));
    //ofLogNotice("(ordering " + ofToString(screens.size() - 1) + ")");
    for (unsigned int i = 0; i < screens.size(); i++) {
        ofLogNotice("--- screen: " + ofToString(i + 1) + " (index " + ofToString(i) + "): ---");
        ofLogNotice("show Warp grid: " + ofToString(screens[i]->getShowWarpGrid()));
        ofLogNotice("points (x/y): " + ofToString(screens[i]->getNumXPoints()) + " / " + ofToString(screens[i]->getNumYPoints()));
        ofLogNotice("resolution: " + ofToString(screens[i]->getWarpGridResolution()));
        ofLogNotice("offset: " + ofToString(screens[i]->getOffset()));
        ofLogNotice("width: " + ofToString(screens[i]->getWidth()));
        ofLogNotice("------");

    }
    ofLogNotice("-------------------------------");
}

// -----------------------------------

void oeScreenManager::addScreen(ofPoint position, int width, int height, int pointsX, int pointsY, int pixelsPerGridDivision) {
    oeScreen* warp = new oeScreen();
    warp->allocate(width, height, pointsX, pointsY, pixelsPerGridDivision, 6408);
    warp->init(width, height);
    warp->setShowWarpGrid(0);
    screens.push_back(warp);
    ordering.push_back(screenNumberCounter);
    screenNumberCounter++;
    // note: ordering holds index for screenNumbers not screenNumber (=screenNumber -1)

    screenPosition.push_back(position);


    ofLogNotice("oeScreenManager: added Screen Nr. " + ofToString(screens.size()) + " - index " + ofToString(screens.size() - 1));
    ofLogNotice("ordering : " + ofToString(ordering));
    screenNumber = ofToString(getScreenCount());
}


// -----------------------------------

void oeScreenManager::removeScreen(int screenNumber) {
    ofLogNotice("try to remove screen " + ofToString(screenNumber));
    screens.erase(screens.begin() + (screenNumber - 1));
    screenNumberCounter--;
    screenPosition.erase(screenPosition.begin() + (screenNumber - 1));
    ordering.erase(ordering.begin() + (screenNumber - 1));
}

// -----------------------------------

void oeScreenManager::toggleGrid(int screenNumber) {
    screens[screenNumber - 1]->toggleShowWarpGrid();
}


// -----------------------------------

void oeScreenManager::toggleWarp(int screenNumber) {
    screens[screenNumber - 1]->setDoWarp(!screens[screenNumber - 1]->getDoWarp() );
}

// -----------------------------------

bool oeScreenManager::getGridState(int screenNumber) {
    return screens[screenNumber - 1]->getShowWarpGrid();
}

// -----------------------------------

void oeScreenManager::moveScreenToFront(int screenNumber) {

    ofLogNotice("screenNumber: " + ofToString(screenNumber));

    ofLogNotice("ordering vorher: " + ofToString(ordering));
    // order of drawing is done by ordering
    unsigned int indexOfScreen;
    for (unsigned int i = 0; i < ordering.size(); i++) {
        if (ordering[i] == screenNumber - 1) {
            indexOfScreen = i;
        }

    }

    ordering.push_back(ordering[indexOfScreen]);
    ofLogNotice("ordering dazwischen: " + ofToString(ordering));

    ordering.erase(ordering.begin() + indexOfScreen);

    ofLogNotice("ordering nacher: " + ofToString(ordering));


}

// -----------------------------------

void oeScreenManager::update() {
    for (unsigned int i = 0; i < screens.size(); i++) {

        screens[i]->update();

//        //maskShader.begin();		
//
//        //shader.setUniform1f( "time", time );	//Pass float parameter "time" to shader
//        //Pass mask to shader (fbo2)
//        //maskShader.setUniformTexture( "texture1", mask.getTextureReference(), 1 ); 
//        //Last parameter 1 is OpenGL texture identifier 
//
//        //        bool drawTargetContentAlso = false;
//        //        ofFbo tempFbo;
//        //        tempFbo.allocate(getScreenWidthOfScreen(i + 1), getScreenHeightOfScreen(i + 1), GL_RGBA);
//
//        screens[i]->getContent().draw(0, 0, getScreenWidthOfScreen(i + 1), getScreenHeightOfScreen(i + 1));
//
//
//
//        //maskShader.end();
//        screens[i]->end();
//        //ofLogNotice("gridDivisionX von " + ofToString(i) + ": " + ofToString(screens[i]->getGridDivisionsX()));
    }
}


// -----------------------------------

void oeScreenManager::draw() {

    if (getScreenCount() > 0) {
        if (actualScreenSoloMode) {
            if (actualScreen > 0) {
                // hack we have to do. See: https://forum.openframeworks.cc/t/ofxbezierwarp-second-warp-object-gl-problem/28624/2
                screens[actualScreen - 1]->setWarpGridResolution(10);
                screens[actualScreen - 1]->oedraw(screenPosition[actualScreen - 1].x, screenPosition[actualScreen - 1].y, screens[actualScreen - 1]->getWidth(), screens[actualScreen - 1]->getHeight());
            } else {
                ofLogWarning("solo Mode but no screen chosen!");
            }
        } else {
            for (unsigned int i = 0; i < ordering.size(); i++) {
                // hack we have to do. See: https://forum.openframeworks.cc/t/ofxbezierwarp-second-warp-object-gl-problem/28624/2
                screens[ordering[i]]->setWarpGridResolution(10);
                screens[ordering[i]]->oedraw(screenPosition[ordering[i]].x, screenPosition[ordering[i]].y, screens[ordering[i]]->getWidth(), screens[ordering[i]]->getHeight() );
            }
        }
    }
}

// -----------------------------------

int oeScreenManager::getScreenNumXPoints(int screenNumber) {
    return screens[screenNumber - 1]->getNumXPoints();
}

int oeScreenManager::getScreenNumYPoints(int screenNumber) {
    return screens[screenNumber - 1]->getNumYPoints();
}

// -----------------------------------

ofPoint oeScreenManager::getPosition(int screenNumber) {
    return screenPosition[screenNumber - 1];
}

void oeScreenManager::setPosition(int screenNumber, ofPoint position) {
    ofLogVerbose("set Screen Position");
    screenPosition[screenNumber - 1] = position;
    setWarpGridPosition();
}

void oeScreenManager::setScreenWidth(int screenNumber, int transWidth) {
    ofLogVerbose("set Screen  width");
    screens[screenNumber - 1]->setWidth(transWidth);
    setWarpGridPosition();

}

int oeScreenManager::getScreenWidth(int screenNumber) {
    return screens[screenNumber - 1]->getWidth();
}

void oeScreenManager::setScreenHeight(int screenNumber, int transHeight) {
    ofLogVerbose("set Screen  height");
    screens[screenNumber - 1]->setHeight(transHeight    );
    setWarpGridPosition();

}

int oeScreenManager::getScreenHeight(int screenNumber) {
    return screens[screenNumber - 1]->getHeight();
}

// -----------------------------------

int oeScreenManager::getScreenWarpGridResolution(int screenNumber) {
    return screens[screenNumber - 1]->getWarpGridResolution();
}


// -----------------------------------

unsigned int oeScreenManager::getScreenCount() {
    return screens.size();
}


string oeScreenManager::getSourceInfo(int screenNumber) {
    return screens[screenNumber - 1]->getSourceInfo();
}

//--------------------------------------------------------------

string oeScreenManager::getTargetSourceInfo(int screenNumber) {
    return screens[screenNumber - 1]->getTargetSourceInfo();
}

int oeScreenManager::getSourceType(int screenNumber) {
    return screens[screenNumber - 1]->getSourceType();
}


//--------------------------------------------------------------

void oeScreenManager::keyPressed(int key) {
    ofLogNotice("key Pressed in oeScreenManager. actualScreen is " + ofToString(actualScreen));
    ofLogNotice(ofToString(key));
    switch (key) {
        case OF_KEY_LEFT:
            screenPosition[actualScreen - 1].x--;
            break;
        case OF_KEY_RIGHT:
            screenPosition[actualScreen - 1].x++;
            break;
        case OF_KEY_UP:
            screenPosition[actualScreen - 1].y--;
            break;
        case OF_KEY_DOWN:
            screenPosition[actualScreen - 1].y++;
            break;
    }
    setWarpGridPosition();
    //screens[actualScreen - 1]->setWarpGridPosition(screenPosition[actualScreen - 1].x, screenPosition[actualScreen - 1].y, screens[actualScreen - 1]->getWidth(), screens[actualScreen - 1]->getHeight());
}


//--------------------------------------------------------------

void oeScreenManager::setWarpGridPosition() {
    ofLogNotice("setWarpGridPosition in oeScreenManager. actualScreen is " + ofToString(actualScreen));

    if (actualScreen) {
        if (showGridOnControlScreen) {
            screens[actualScreen - 1]->setWarpGridPosition(100, 100, (firstMonitorWidth - 200), (firstMonitorHeight - 200));
            ofLogNotice("set warp Grid to 100, 100, " + ofToString((firstMonitorWidth - 200)) + ", " + ofToString((firstMonitorHeight - 200)));
        } else {
            screens[actualScreen - 1]->setWarpGridPosition(screenPosition[actualScreen - 1].x, screenPosition[actualScreen - 1].y, screens[actualScreen - 1]->getWidth(), screens[actualScreen - 1]->getHeight() );
        }
    }
}

//--------------------------------------------------------------
// prepare the save of warp settings

void oeScreenManager::saveWarpSettings() {
    ofLogNotice("------- save WarpSettings! --------");

    string path = "screenManager/warpGrid/" + ofToString(actualScreen);
    ofDirectory actWarpDir(path);

    if (!actWarpDir.exists()) {
        actWarpDir.create(true);
    }
    saveWarpSettingsToXML(path + "/grid.xml");
}



//--------------------------------------------------------------
// actual saving of warp settings

void oeScreenManager::saveWarpSettingsToXML(string path) {
    vector<GLfloat> controlPoints;

    controlPoints = screens[actualScreen - 1]->getControlPoints();

    auto tempSettings = warpXml.getChild("SETTINGS");
    if(!tempSettings){
            tempSettings = warpXml.appendChild("SETTINGS");
    }
    tempSettings.removeChild("CONTROLPOINTS");
    auto tempPoints = tempSettings.appendChild("CONTROLPOINTS");


    ofLogNotice("XML-Datei für Warp geleert. Nun neu anlegen...");

    ofLogNotice("control points: " + ofToString(controlPoints.size()));

    for (unsigned int i = 0; i < controlPoints.size(); i++) {
        auto onePoint = tempPoints.appendChild("PT");
        onePoint.set(controlPoints[i]);
    }

    ofLogNotice("XML-Datei für Warp angelegt");

    warpXml.save(path);
    ofLogNotice("XML-Datei für Warp gespeichert: " + path);

}

//--------------------------------------------------------------

int oeScreenManager::countPointsInSavedWarpSettings(string path) {

    if (warpXml.load(path)) {
        ofLogNotice("XML-Datei für Warp geladen. Pfad:" + ofToString(path));
    } else {
        ofLogWarning("XML-Datei für Warp existiert nicht? Pfad:" + ofToString(path) + ". Warp-Punkte können nicht geladen werden");
        return 0;
    }
    auto tempSettings = warpXml.getChild("SETTINGS");
    auto tempPoints = tempSettings.getChild("CONTROLPOINTS");
    auto tempAllPoints = tempPoints.getChildren("PT");
    int number = 0;
    for (auto & pt : tempAllPoints) {
        number++;
    }
    return number;
}


//--------------------------------------------------------------

void oeScreenManager::rearrangeAllPoints(int screenNumber) {
    screens[screenNumber - 1]->rearrangeAllPoints();
}

//--------------------------------------------------------------

void oeScreenManager::resetWarpGrid(int screenNumber) {
    screens[screenNumber - 1]->resetWarpGrid();
}

//--------------------------------------------------------------

void oeScreenManager::loadWarpSettings(bool forceLoad) {
    loadWarpSettingsFromXML("screenManager/warpGrid/" + ofToString(actualScreen) + "/grid.xml", forceLoad);
}

//--------------------------------------------------------------

void oeScreenManager::moveCorner(int screenNumber, ofOeCorner corner, ofOeDirection direction) {
    screens[screenNumber - 1]->moveCorner(corner,direction);
}

//--------------------------------------------------------------

void oeScreenManager::loadWarpSettingsFromXML(string path, bool forceLoad) {
    //we load our  file
    int numberofPoints = countPointsInSavedWarpSettings(path);
    if (numberofPoints) {
        vector<GLfloat> controlPoints;
        ofLogVerbose("load Warp Settings From XML");
        if (numberofPoints == screens[actualScreen - 1]->getControlPoints().size() || forceLoad) {
            auto tempSettings = warpXml.getChild("SETTINGS");
            auto tempPoints = tempSettings.getChild("CONTROLPOINTS");
            auto tempAllPoints = tempPoints.getChildren("PT");
            int number = 0;
            for (auto & pt : tempAllPoints) {
                controlPoints.push_back(pt.getFloatValue());
            }

            screens[actualScreen - 1]->setControlPoints(controlPoints);
        } else {
            ofLogWarning("Warp-Gitter in settings.xml stimmt nicht mit der Anzahl der gespeicherten Punkte in warp/warp.xml überein! warp-seetings daher nicht automatisch geladen.");
            //screens[actualScreen - 1]->setWarpGrid(warpPointsAmmountX, warpPointsAmmountY);
        }
        ofLogVerbose("load Warp Settings From XML OK");
    } else {
        resetWarpGrid(actualScreen);
    }

}

//--------------------------------------------------------------

void oeScreenManager::setTargetSource(int screenNumber, int type, string path, ofFloatColor color, int fadeTime, ofLoopType loop, float speed, float masterSpeed, float masterAlpha) {
    ofLogNotice("oeScreenManager::setTargetSource for screen " + ofToString(screenNumber) + ", type: " + ofToString(type) + ", path: " + path + + ", fadeTime: " + ofToString(fadeTime) + ", loop: " + ofToString(loop) + ", speed : " + ofToString(speed) );
    screens[screenNumber - 1]->setTargetSource(type, path, color, fadeTime, loop, speed, masterSpeed, masterAlpha);
}



//----------------------------------------

void oeScreenManager::setMasterAlpha(int screenNumber, int talpha) {
    screens[screenNumber - 1]->setMasterAlpha(talpha);
}

//----------------------------------------

int oeScreenManager::getMasterAlpha(int screenNumber) {
    return screens[screenNumber - 1]->getMasterAlpha();
}


//----------------------------------------

ofVec3f oeScreenManager::getTintColor(int screenNumber) {
    return screens[screenNumber - 1]->tintColor;
}

//----------------------------------------

void oeScreenManager::setTintColor(int screenNumber, ofVec3f transColor) {
    screens[screenNumber - 1]->tintColor = transColor;
}

//----------------------------------------

float oeScreenManager::getBrightness(int screenNumber) {
    return screens[screenNumber - 1]->brightness;    
}

//----------------------------------------------------------

void oeScreenManager::setBrightness(int screenNumber, float transBrightness) {
    screens[screenNumber - 1]->brightness = transBrightness;
}

//----------------------------------------------------------

float oeScreenManager::getContrast(int screenNumber) {
    return screens[screenNumber - 1]->contrast;    
}

//----------------------------------------------------------

void oeScreenManager::setContrast(int screenNumber, float transContrast) {
    screens[screenNumber - 1]->contrast = transContrast;
}

//----------------------------------------------------------

float oeScreenManager::getSaturation(int screenNumber) {
    return screens[screenNumber - 1]->saturation;    
}

//----------------------------------------------------------

void oeScreenManager::setSaturation(int screenNumber, float transSaturation) {
    screens[screenNumber - 1]->saturation = transSaturation;
}

//--------------------------------------------------------------

bool oeScreenManager::getVideoPaused(int screenNumber) {
    return screens[screenNumber - 1]->getVideoPaused();
}

//--------------------------------------------------------------

ofLoopType oeScreenManager::getVideoLoopState(int screenNumber) {
    return screens[screenNumber - 1]->getVideoLoopState();
}

//--------------------------------------------------------------

float oeScreenManager::getVideoSpeed(int screenNumber) {
    return screens[screenNumber - 1]->getVideoSpeed();
}  

//--------------------------------------------------------------

void oeScreenManager::setVideoSpeed(int screenNumber, float speed, float masterSpeed) {
    screens[screenNumber - 1]->setVideoSpeed(speed, masterSpeed);
}


//--------------------------------------------------------------

float oeScreenManager::getVideoPosition(int screenNumber) {
    return screens[screenNumber - 1]->getVideoPosition();
}  

//--------------------------------------------------------------

void oeScreenManager::setVideoPosition(int screenNumber, float position) {
    screens[screenNumber - 1]->setVideoPosition(position);
}

//--------------------------------------------------------------

string oeScreenManager::getMaskPath(int screenNumber) {
    return screens[screenNumber - 1]->getMaskPath();
}

//--------------------------------------------------------------

void oeScreenManager::setMask(int screenNumber, string maskPath) {
    ofLogNotice("try to set Mask " + maskPath + " for screen " + ofToString(screenNumber) );
    screens[screenNumber - 1]->setMask(maskPath);    
}

//--------------------------------------------------------------

void oeScreenManager::setMaskPreview(int screenNumber, ofImage maskPreview) {
    ofLogNotice("try to set Mask Preview for screen " + ofToString(screenNumber) );
    screens[screenNumber - 1]->setMaskPreview(maskPreview);    
}

//--------------------------------------------------------------

void oeScreenManager::removeMaskPreview(int screenNumber) {
    ofLogNotice("removeMaskPreview for screen " + ofToString(screenNumber));
    screens[screenNumber - 1]->removeMaskPreview();
}



//--------------------------------------------------------------

ofFloatColor oeScreenManager::getColor(int screenNumber) {
    return screens[screenNumber - 1]->getColor();
}

string oeScreenManager::getPath(int screenNumber) {
    return screens[screenNumber - 1]->getPath();
}

int oeScreenManager::getFadeTime(int screenNumber) {
    return screens[screenNumber - 1]->getFadeTime();
}