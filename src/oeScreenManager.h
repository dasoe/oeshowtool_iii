#pragma once
#include "ofMain.h"

#include "oeScreen.h"
#include "ofxXmlSettings.h"

class oeScreenManager {
public:
    //oeScreenManager();
    void setup(int firstMonitorWidthTemp, int firstMonitorHeightTemp, int contentWidthTemp, int contentHeightTemp);
    void update();
    void draw();
    
    void info();
    void moveScreenToFront(int screenNumber);
    void keyPressed(int key);

    void showGridOnControlScreenChanged(bool & active);
    void moveCorner(int screenNumber, ofOeCorner corner, ofOeDirection direction);
    void rearrangeAllPoints(int screenNumber);

    unsigned int getScreenCount();
    int getScreenNumYPoints(int screenNumber);
    int getScreenNumXPoints(int screenNumber);
    int getScreenWarpGridResolution(int screenNumber);
    bool getGridState(int screenNumber);
    void setTargetSource(int screenNumber, int type, string path, ofFloatColor color, int fadeTime, ofLoopType loop, float speed, float masterSpeed, float masterAlpha);
    string getTargetSourceInfo(int screenNumber);
    string getSourceInfo(int screenNumber);
    int getSourceType(int screenNumber);
    
    
    ofPoint getPosition(int screenNumber);  
    void setPosition(int screenNumber, ofPoint position);
    
    int getScreenWidth(int screenNumber);
    void setScreenWidth(int screenNumber, int transWidth);
    
    int getScreenHeight(int screenNumber);
    void setScreenHeight(int screenNumber, int transHeight);

    void toggleGrid(int screenNumber);
    void toggleWarp(int screenNumber);
    
    void addScreen(ofPoint position, int width, int height, int pointsX, int pointsY, int pixelsPerGridDivision);
    void removeScreen(int screenNumber);
    void resetWarpGrid(int screenNumber);
    
    void setWarpGridPosition();
    void saveWarpSettings();
    void saveWarpSettingsToXML(string path);
    void loadWarpSettings(bool forceLoad);
    void loadWarpSettingsFromXML(string path, bool forceLoad);
    int countPointsInSavedWarpSettings(string path);
    
    float getBrightness(int screenNumber);
    void setBrightness(int screenNumber, float transBrightness);

    float getContrast(int screenNumber);
    void setContrast(int screenNumber, float transContrast);

    float getSaturation(int screenNumber);
    void setSaturation(int screenNumber, float transSaturation);

    ofVec3f getTintColor(int screenNumber);
    void setTintColor(int screenNumber, ofVec3f transColor);

    bool getVideoPaused(int screenNumber);  

    float getVideoSpeed(int screenNumber);  
    ofLoopType getVideoLoopState(int screenNumber);  
    void setVideoSpeed(int screenNumber, float speed, float masterSpeed);
    
    float getVideoPosition(int screenNumber);  
    void setVideoPosition(int screenNumber, float position);
    
    string getMaskPath(int screenNumber);

    void setMasterAlpha(int screenNumber, int talpha);
    int getMasterAlpha(int screenNumber);


    void setMask(int screenNumber, string maskPath);
    void setMaskPreview(int screenNumber, ofImage maskPreview);
    void removeMaskPreview(int screenNumber);
    
    ofFloatColor getColor(int screenNumber);
    string getPath(int screenNumber);
    int getFadeTime(int screenNumber);
    
    vector <ofFbo> content;
    vector <oeScreen*> screens;
    vector <ofPoint> screenPosition;

    vector <int> ordering;

    ofxXmlSettings gridData;
    ofXml warpXml;

    ofShader maskShader; //Shader


    unsigned int actualScreen, screenNumberCounter, firstMonitorWidth, firstMonitorHeight, contentWidth, contentHeight;
    bool actualScreenSoloMode, showGridOnControlScreen;
    
    string screenNumber, activeScreen;    
};