#include "oeSlot.h"


//--------------------------------------------------------------

void oeSlot::init() {
    
    rewindOnTransition = true;
    mainVideo = 0;
    contrast = 1.0;
    brightness = 1.0;
    saturation = 1.0;
    type = OE_IMAGE;
    color = ofFloatColor(0.4, 0.5, 0.2, 1.0);
}

//--------------------------------------------------------------

void oeSlot::setSlotType(oeSlotType transType) {
    type = transType;
}


//--------------------------------------------------------------

void oeSlot::setSlotType(int transType) {
    type = (oeSlotType) transType;
}

//--------------------------------------------------------------

void oeSlot::setSlotPath(string transPath) {
    path = transPath;
}


//--------------------------------------------------------------

string oeSlot::getSlotPath() {
    return path;
}

//--------------------------------------------------------------

string oeSlot::getInfo() {
    string info = "";
    if (getSlotType() == OE_COLOR) {
        info = "COLOR ";
    } else if (getSlotType() == OE_IMAGE) {
        string cleanedPath = path;
        ofStringReplace(cleanedPath, "images/", "");
        info = "IMAGE | " + cleanedPath;
    } else if (getSlotType() == OE_VIDEO) {
        string cleanedPath = path;
        ofStringReplace(cleanedPath, "videos/", "");
        info = "VIDEO | " + cleanedPath;
    }
    else if (getSlotType() == OE_EXTERNAL) {
        string cleanedPath = path;
        ofStringReplace(cleanedPath, "external/", "");
        info = "EXTERNAL | " + cleanedPath;
    }
    return info;
}


//--------------------------------------------------------------

string oeSlot::getMoviePath() {
        return "";
}



//--------------------------------------------------------------

oeSlotType oeSlot::getSlotType() {
    return type;
}

//--------------------------------------------------------------

oeSlotType oeSlot::getTargetSlotType() {
    return targetType;
}


//--------------------------------------------------------------

bool oeSlot::getIsActive() {
    actualTime = ofGetElapsedTimeMillis();
    if (actualTime - slotIsActiveTimer > 300) {
        return false;
    } else {
        return true;
    }
}


//--------------------------------------------------------------

void oeSlot::update() {
    actualTime = ofGetElapsedTimeMillis();
}



//--------------------------------------------------------------

void oeSlot::setVideoLoopState(ofLoopType state) {
    videoLoopstate = state;
}

ofLoopType oeSlot::getVideoLoopState() {
    return videoLoopstate;
}
//--------------------------------------------------------------

void oeSlot::setTargetVideoLoopState(ofLoopType state) {
}

//--------------------------------------------------------------

void oeSlot::restartVideo() {
}

//--------------------------------------------------------------

void oeSlot::restartTargetVideo() {
}

//--------------------------------------------------------------

void oeSlot::setVideoPaused(bool paused) {
}

//--------------------------------------------------------------

void oeSlot::setVideoSpeed(float speed) {
    videoSpeed = speed;
}

float oeSlot::getVideoSpeed() {
    return videoSpeed;
}


//--------------------------------------------------------------

void oeSlot::setVideoPosition(float percent) {
}

//--------------------------------------------------------------

float oeSlot::getVideoPosition() {
    return 0;
}

//--------------------------------------------------------------

void oeSlot::selectSource() {

}

//--------------------------------------------------------------

void oeSlot::startTransitionForSlot() {
}

//--------------------------------------------------------------
