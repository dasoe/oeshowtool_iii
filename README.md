# oeShowTool III

This tool is used for driving live shows based on video projection. There's wonderful Tools out there, most of them very professional. But most of them also need a lot of preparation. As I often do more or less improvised shows on subculture events, I needed another approach: Very quick (and dirty?) setup, possibility to drive decent show with little to nearly no preparation. 
This is what this Tool is for.

(Before, I used the most impressive [VPT8](https://hcgilje.wordpress.com/vpt/). In case you are interested, find a comparison of the approaches [here](Comparison-VPT.md).)

## Features

- set up virtual screens quickly and directly in the room, even without any knowledge of measures etc.
	- Position
	- Advanced warping & mapping
	- Masks
- Distribute Sources to the screens using fades
	+ Colors
	+ Images
	+ Videos
	+ Exernal from other Tools (only Windows, using Spout at the moment)
- Prepare scenes, independently of the screens setup

## Installation
Please go [here](Install.md) for Insructions.

## Dependencies

#Windows

You will have to install gStreamer.

You get the installers here (https://gstreamer.freedesktop.org/data/pkg/windows/). You will need the base installer (like gstreamer-1.0-x86-1.6.4.msi) and the devel installer (like gstreamer-1.0-devel-x86-1.6.4.msi). For 64bit you need the appropriate ones (x86_64).
I used Version 1.6.4. as 1.7.1 crashed on my system when decoding h264. But (installer!) it is easy to install and uninstall in case you want to try another version.
please install the complete package, otherwise it might not work later on with openFrameworks.
On my system the installer wanted to use the second disk for the installation path. To prevent confusion I suggest to change this: In the installer click “custom” and validate the path. If neccessary change it to default (C:\gstreamer). You can then go back and click “complete”.

You then have installed gstreamer on your system. The installer took care of the path variable. You can try if it works by opening a command prompt and typing sth. like “gst-play-1.0 YOURMOVIE.mp4”.

#Linux
sudo apt-get install libgtk2.0-dev

	
## Status: Beta. Basic functionality is ready:
The tool has been successfully used for driving live shows. Use tag 0.1.1.beta.

- Screen management
	- Advanced mapping and warping
	- Masks
	- Brightness/contrast/saturation shader
- Content slots: Color, Image, Video
- Content to screens (cross fade)
- Scenes
- Mask Editor 
	- On-the-fly Bezier mask generation, saveable
	- Generate pixel Mask from Bezier (blur possible)
- Master Faders for Speed of all Videos and Transparency of all screens

External Content sources: Spout for Windows is included and working (see Tag 0.1.1.beta). Actual version is implementing NDI (fo all Systems). It's abit tricky, though.
Eventually hopefully also Syphon for OS X (I quit working on Mac, though). 

## New stuff: 0.1.2.beta
- added docking to the GUI (ImGui).
- started working on KeyStrokes (greeting to Leeuwarden to Marinus!). This will take some time.

## Future
- Timeline. Yet unclear how to best do it.

---

This is an **openFrameworks** Tool. Please visit http://openframeworks.cc to download it. In case you don't know it: it is very worth checking it out!

The Tool is heavily based on the following excellent external addons:
- ofxBezierWarp (https://github.com/gameoverhack/ofxBezierWarp) for mapping/warping
- ofxImGui (https://github.com/jvcleave/ofxImGui) which again is a wrapper for ImGui (https://github.com/ocornut/imgui) for the GUI
- ofxBezierEditor (https://github.com/acarreras/ofxBezierEditor) for the bezier mask generation

As of internal addons the following are vitally used:
- ofxOpenCv for Image manipulation
- ofxXmlSettings for saving stuff

For debugging I used ofxTimeMeasurements (https://github.com/armadillu/ofxTimeMeasurements), I include it still, as it was so useful in finding a bottleneck once.

**Thanks a lot to the authors for the incredible work!**

I also (eventually) use the following own addons:

- ofxXmlBasedProjectSettings
- ofxFlashMessages



---
# TODO:

- descriptions/help
- test run and readjust GUI to the needs (more descriptive)
- error catching
- check multi monitors on ubuntu (3+)
- windows version using gstreamer (as I had performance issues with the internal video processing)?
- Priority 3: 
	- DMX channels for connection of (arduino-driven) lighting
	- and/or: basic GPIO solution
	- see VPT8: flexible MIDI controlling (tools like KORG nanoKontrol)
	- Generally: All-purpose-Sliders for setting multiple pre-set Sliders? Maybe to be assigned by just clicking?


# Screenshots
App spans multiple screens, all but the main (operating) screen being projectors

[![screenshot multiple_screens](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/1_screen-pos-warp.jpg)](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/1_screen-pos-warp.jpg)
Warp a screen


[![screenshot warping_a_screen](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/2_mask-creation.jpg?ref_type=heads)](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/2_mask-creation.jpg?ref_type=heads)
Live mask generation (can be saved and used as pixel mask in the end)


[![screenshot building_a_mask](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/3_during_show.jpg?ref_type=heads)](https://gitlab.com/dasoe/oeshowtool_iii/-/raw/main/screenshots/3_during_show.jpg?ref_type=heads)
During a show


---
# Building yourself:

## openFrameworks
[Download openFrameworks 0.12](https://openframeworks.cc/download/) and set it up depending on your system (mainly: installing VisualStudio on Windows, running 2 scripts on Linux or Downloading XCode on Mac, find detailed explanation ot the site you donloaded from)

## Tool
- https://gitlab.com/dasoe/oeshowtool_iii. Download and unpack or git clone in the openFrameworks-Folder `"apps/myApps"`

Don't know about the naming Problem. It should be `oeShowtool_III` (note the uppercase Letters)

Dowload and unpack or git clone the following addons in the openFrameworks-Folder `"addons"`:

## Addons
- https://github.com/dasoe/ofxBezierWarp (forked from [gameoverhack](https://github.com/gameoverhack)
- https://github.com/dasoe/ofxBezierEditor (forked from [acarreras](https://github.com/acarreras)
- https://github.com/dasoe/ofxFlashMessages
- https://github.com/dasoe/ofxXmlBasedProjectSettings
- https://github.com/armadillu/ofxTimeMeasurements
- https://github.com/jvcleave/ofxImGui ("develop" branch for docking, hence from 0.1.2.beta)

Following addons are part of openFrameworks. They already should be present in the mentioned folder:
- ofxXmlSettings
- ofxOpenCv

## Preparation
On Windows you should (need to?) set the target to `64 bit` and `Release`. 
The Build.

On Linux just enter the folder of the Tool in a Terminal and type `make`.

## Build Troubles
In case this does not work out, try the following

Check the name of the Folder of the Tool. It should be `oeShowtool_III` (note the uppercase Letters).

All systems: Close the IDE in case it is open. Open `projectGenerator` of openFrameworks and use it to open the Folder of the tool. Then check, if the following addons are listed and no problems are marked.
- ofxBezierEditor
- ofxBezierWarp
- ofxFlashMessages
- ofxImGui
- ofxOpenCv
- ofxTimeMeasurements
- ofxXmlBasedProjectSettings
- ofxXmlSettings

Then click on `update`.

Windows: Open the Solution of the Tool in VisualStudio. Right-click on the Solution and do `retarget Solution`, so that The Tool and openFrameworks have the same target SDK set.

Then rebuild


